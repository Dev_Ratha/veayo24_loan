﻿using System.Threading.Tasks;
using Veayo24.PS.Lib.OneSignal;

namespace Veayo24.PS.Infrastructure.OneSignal
{
    public class NotificationDb : INotificationDb
    {


        public NotificationDb()
        {

        }

        public Task SaveAsync(string userId, ONotificationBase noti)
        {
            return Task.CompletedTask;
        }

        public Task SaveAsync(string userId, INotification noti, string notificationId = "")
        {
            return Task.CompletedTask;
        }
    }
}
