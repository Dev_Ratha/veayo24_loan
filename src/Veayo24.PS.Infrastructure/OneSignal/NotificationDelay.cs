﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Mip.Lib.OneSignal;
using Mip.Lib.Redis;

namespace Mip.Infrastructure.OneSignal
{
    public class NotificationDelay : INotificationQueue
    {
        private readonly IRedisCache _redis;

        public NotificationDelay(IRedisCache redis)
        {
            _redis = redis;
        }

        public Task SendQueueAsync(string userId, ONotificationBase noti, TimeSpan delay)
        {
            throw new NotImplementedException();
        }
    }
}
