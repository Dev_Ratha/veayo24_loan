﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Infrastructure.SQLServers;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.EntityFramework
{
    public class EntityContextRepository<T> : IEntityContextRepository<T> where T : BaseTable
    {
        public SqlDbContext Context { get; }
        public DbContext db => Context;
        public DbSet<T> GetDbSet<TEntity>() where TEntity : BaseTable
            => db.Set<T>();

        public EntityContextRepository(SqlDbContext dbContext)
        {
            Context = dbContext;
        }
        public T Add(T entity)
        {
            db.Entry(entity).State = EntityState.Added;
            db.SaveChanges();
            return entity;
        }

        public async Task<T> AddAsync(T entity)
        {
            db.Entry(entity).State = EntityState.Added;
            await db.SaveChangesAsync();
            return entity;
        }

        public IQueryable<T> All()
        {
            return db.Set<T>().AsNoTracking();
        }

        public T Update(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.Set<T>().Update(entity);
            db.SaveChanges();
            return entity;
        }

        public async Task<T> UpdateAsync(T entity)
        {
            db.Entry(entity).State = EntityState.Modified;
            db.Set<T>().Update(entity);
            await db.SaveChangesAsync();
            return entity;
        }

        public IQueryable<T> Find(Expression<Func<T, bool>> predicate)
        {
            return db.Set<T>().Where(predicate).AsNoTracking();
        }

        public int Delete(T entity)
        {
            db.Set<T>().Remove(entity);
            return db.SaveChanges();
        }

        public Task<int> DeleteAsync(T entity)
        {
            db.Set<T>().Remove(entity);
            return db.SaveChangesAsync();
        }

        public Task<int> DeleteAsync(Expression<Func<T, bool>> predicate)
        {
            var collection = db.Set<T>().Where(predicate);
            db.Set<T>().RemoveRange(collection);
            return db.SaveChangesAsync();
        }
    }
}
