﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.UserRoles;

namespace Veayo24.PS.Infrastructure.SQLServers.UserRoles
{
    public static class Extensions
    {
        public static UserRoleTable AsTable(this UserRoleEntity x)
            => new UserRoleTable()
            {
                Id = x.Id,
                Role = x.Role,
                BranchId = x.BranchId,
            };

        public static UserRoleEntity AsEntity(this UserRoleTable x)
            => new UserRoleEntity(id: x.Id, role: x.Role, branchId: x.BranchId);
    }
}
