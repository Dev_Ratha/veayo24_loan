﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers.UserRoles
{
    [Table("UserRoles")]
    public class UserRoleTable : BaseTable
    {
        public string Role { get; set; }
        public long? BranchId { get; set; }
    }
}
