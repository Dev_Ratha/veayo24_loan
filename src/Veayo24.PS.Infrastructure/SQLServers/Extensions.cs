﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Core.Exceptions;
using Veayo24.PS.Infrastructure.EntityFramework;
using Veayo24.PS.Infrastructure.SQLServers.CreditOfficers;
using Veayo24.PS.Infrastructure.SQLServers.UserRoles;
using Veayo24.PS.Infrastructure.SQLServers.Users;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers
{
    public static class Extensions
    {
        public static IServiceCollection AddDbContextRepositories(this IServiceCollection services)
        {
            services.AddDbContextRepository<UserTable>();
            services.AddDbContextRepository<UserRoleTable>();
            services.AddDbContextRepository<CreditOfficerTable>();
            return services;
        }


        public static IServiceCollection AddDbContextRepository<TEntity>(this IServiceCollection services) where TEntity : BaseTable
        {
            services.AddTransient<IEntityContextRepository<TEntity>>(sp =>
            {
                var options = services.BuildServiceProvider().GetService<DbContextOptions<SqlDbContext>>();
                if (options is null)
                    throw new ServiceRequiredException("DbContextOptions");
                return new EntityContextRepository<TEntity>(new SqlDbContext(options));
            });

            return services;
        }
    }
}
