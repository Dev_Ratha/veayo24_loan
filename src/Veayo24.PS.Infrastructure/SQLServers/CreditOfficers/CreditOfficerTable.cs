﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers.CreditOfficers
{
    [Table("CreditOfficers")]
    public class CreditOfficerTable : BaseTable
    {
        public string Name { get; set; }
        public string NameLatin { get; set; }
        public string Position { get; set; }
        public bool IsProbation { get; set; }
        public bool IsLRO { get; set; }
        public bool IsActive { get; set; }
        public long BranchId { get; set; }
        public DateTime CreatedAt { get; set; }
        public DateTime UpdatedAt { get; set; }
    }
}
