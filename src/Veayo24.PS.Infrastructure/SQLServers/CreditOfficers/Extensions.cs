﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.CreditOfficers;

namespace Veayo24.PS.Infrastructure.SQLServers.CreditOfficers
{
    public static class Extensions
    {
        public static CreditOfficerTable AsTable(this CreditOfficerEntity x)
            => new CreditOfficerTable
            {
                Id = x.Id,
                Name = x.Name,
                NameLatin = x.NameLatin,
                IsActive = x.IsActive,
                CreatedAt = x.CreatedAt,
                IsLRO = x.IsLRO,
                IsProbation = x.IsProbation,
                Position = x.Position,
                UpdatedAt = x.UpdatedAt,
                BranchId = x.BranchId
            };

        public static CreditOfficerEntity AsEntity(this CreditOfficerTable x)
            => new CreditOfficerEntity(
                id: x.Id,
                name: x.Name,
                nameLatin: x.NameLatin,
                isActive: x.IsActive,
                createdAt: x.CreatedAt,
                isLRO: x.IsLRO,
                isProbation: x.IsProbation,
                position: x.Position,
                updatedAt: x.UpdatedAt,
                branchId: x.BranchId
                );
    }
}
