﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Veayo24.PS.Infrastructure.SQLServers.CreditOfficers;
using Veayo24.PS.Infrastructure.SQLServers.UserRoles;
using Veayo24.PS.Infrastructure.SQLServers.Users;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers
{
    public class SqlDbContext : DbContext
    {
        public DbSet<UserTable> Users { get; set; }

        public DbSet<UserRoleTable> UserRoles { get; set; }
        public DbSet<CreditOfficerTable> CreditOfficers { get; set; }
        public SqlDbContext(DbContextOptions<SqlDbContext> options) : base(options)
        {
        }
    }
}
