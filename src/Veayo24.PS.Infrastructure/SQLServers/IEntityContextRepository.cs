﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Infrastructure.SQLServers;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.EntityFramework
{
    public interface IEntityContextRepository<T> where T : BaseTable
    {
        SqlDbContext Context { get; }
        T Add(T entity);
        Task<T> AddAsync(T entity);
        IQueryable<T> All();
        T Update(T entity);
        Task<T> UpdateAsync(T entity);
        IQueryable<T> Find(Expression<Func<T, bool>> predicate);
        int Delete(T entity);
        Task<int> DeleteAsync(T entity);
        Task<int> DeleteAsync(Expression<Func<T, bool>> predicate);
    }
}
