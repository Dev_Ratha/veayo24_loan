﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers.Users
{
    [Table("Users")]
    public class UserTable : BaseTable
    {
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long? UserRoleId { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public long? MediaId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long? BranchId { get; set; }
        public bool IsActive { get; set; }
    }
}
