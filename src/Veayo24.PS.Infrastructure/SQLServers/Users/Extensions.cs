﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.Users;

namespace Veayo24.PS.Infrastructure.SQLServers.Users
{
    public static class Extensions
    {
        public static UserTable AsTable(this UserEntity x)
             => new UserTable
             {
                 Id = x.Id,
                 FullName = x.FullName,
                 UserName = x.UserName,
                 BranchId = x.BranchId,
                 CreatedAt = x.CreatedAt,
                 MediaId = x.MediaId,
                 Password = x.PasswordHash,
                 Role = x.Role,
                 Token = x.Token,
                 UpdatedAt = x.UpdatedAt,
                 IsActive = x.IsActive,
                 UserRoleId = x.UserRoleId,
             };
        public static UserEntity AsEntity(this UserTable x)
            => new UserEntity(
                    id: x.Id,
                    fullName: x.FullName,
                    userName: x.UserName,
                    branchId: x.BranchId,
                    createdAt: x.CreatedAt,
                    mediaId: x.MediaId,
                    password: x.Password,
                    role: x.Role,
                    token: x.Token,
                    updatedAt: x.UpdatedAt,
                    isActive: x.IsActive,
                    userRoleId: x.UserRoleId
                )
            {
                PasswordHash = x.Password
            };
    }
}
