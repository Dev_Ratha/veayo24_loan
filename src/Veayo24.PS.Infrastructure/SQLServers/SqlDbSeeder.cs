﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Core.Repositories.Users;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.SQLServers
{
    public class SqlDbSeeder : IDBSeeder
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher<UserEntity> _passwordHasher;

        public SqlDbSeeder(IUserRepository userRepository, IPasswordHasher<UserEntity> passwordHasher)
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
        }
        public async Task SeedAsync()
        {
            await SeedDefaultUserAsync();
        }
        public async Task SeedDefaultUserAsync()
        {
            var user = await _userRepository.GetByUserNameAsync("veayo_admin");
            if (user is null)
            {
                user = new UserEntity(0, "Veayo Admin", "veayo_admin", "admin@2022",1, "SAdmin", String.Empty, null, DateTime.Now, DateTime.Now, null, true);
                user.SetPassword("admin@2022", _passwordHasher);
                await _userRepository.AddAsync(user);
            }
        }
    }
}
