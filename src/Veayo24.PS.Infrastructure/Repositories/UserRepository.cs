﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Core.Repositories.Users;
using Veayo24.PS.Infrastructure.EntityFramework;
using Veayo24.PS.Infrastructure.SQLServers.Users;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IEntityContextRepository<UserTable> _repository;

        public UserRepository(IEntityContextRepository<UserTable> repository)
        {
            _repository = repository;
        }
        public Task AddAsync(UserEntity entity)
            => _repository.AddAsync(entity.AsTable());

        public Task DeleteAsync(UserEntity entity)
            => _repository.DeleteAsync(entity.AsTable());

        public Task DeleteAsync(long id)
            => _repository.DeleteAsync(x => x.Id == id);

        public async Task<UserEntity?> GetAsync(long id)
            => (await _repository.Find(x => x.Id == id).FirstOrDefaultAsync())?.AsEntity();

        public async Task<UserEntity?> GetByUserNameAsync(string userName)
        {
            var user = await _repository.Find(x => x.UserName == userName && x.IsActive).FirstOrDefaultAsync();
            return user?.AsEntity();
        }

        public async Task<Pagination<UserEntity>> GetUserPageByBranch(long? branchId, int pageid, int result)
        {
            var page = await _repository.Find(x => x.BranchId == branchId).ToPaginationAsync(pageid, result);
            return page.Map(x => x.AsEntity());
        }

        public Task UpdateAsync(UserEntity entity)
            => _repository.UpdateAsync(entity.AsTable());
    }
}
