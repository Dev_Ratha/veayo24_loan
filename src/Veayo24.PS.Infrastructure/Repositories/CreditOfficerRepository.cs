﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Veayo24.PS.Core.Entities.CreditOfficers;
using Veayo24.PS.Core.Repositories;
using Veayo24.PS.Infrastructure.EntityFramework;
using Veayo24.PS.Infrastructure.SQLServers.CreditOfficers;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure.Repositories
{
    public class CreditOfficerRepository : ICreditOfficerRepository
    {
        private readonly IEntityContextRepository<CreditOfficerTable> _repository;

        public CreditOfficerRepository(IEntityContextRepository<CreditOfficerTable> repository)
        {
            _repository = repository;
        }
        public async Task<CreditOfficerEntity> AddAsync(CreditOfficerEntity creditOfficer)
        {
            var entity = await _repository.AddAsync(creditOfficer.AsTable());
            return entity.AsEntity();
        }

        public async Task<CreditOfficerEntity?> GetAsync(long id)
        {
            var tb = await _repository.Find(x => x.Id == id).FirstOrDefaultAsync();
            return tb?.AsEntity();
        }

        public async Task<Pagination<CreditOfficerEntity>> GetPageAsync(long branchId, int pageId, int result)
        {
            var p = await _repository.Find(x => x.BranchId == branchId).ToPaginationAsync(pageId, result);
            return p.Map(x => x.AsEntity());
        }

        public async Task<CreditOfficerEntity> UpdateAsync(CreditOfficerEntity creditOfficer)
        {
            var tb = await _repository.UpdateAsync(creditOfficer.AsTable());
            return tb.AsEntity();
        }
    }
}
