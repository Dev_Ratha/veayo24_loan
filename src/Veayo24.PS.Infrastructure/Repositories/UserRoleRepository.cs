﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Veayo24.PS.Core.Entities.UserRoles;
using Veayo24.PS.Core.Repositories;
using Veayo24.PS.Infrastructure.EntityFramework;
using Veayo24.PS.Infrastructure.SQLServers.UserRoles;

namespace Veayo24.PS.Infrastructure.Repositories
{
    public class UserRoleRepository : IUserRoleRepository
    {
        private readonly IEntityContextRepository<UserRoleTable> _repository;

        public UserRoleRepository(IEntityContextRepository<UserRoleTable> repository)
        {
            _repository = repository;
        }
        public async Task<UserRoleEntity> AddAsync(UserRoleEntity userRoleEntity)
        {
            var tb = await _repository.AddAsync(userRoleEntity.AsTable());
            return tb.AsEntity();
        }

        public Task DeleteAsync(long id)
            => _repository.DeleteAsync(x => x.Id == id);

        public async Task<UserRoleEntity?> GetAsync(long id)
        {
            var tb = await _repository.Find(x => x.Id == id).FirstOrDefaultAsync();
            return tb?.AsEntity();
        }

        public async Task<IEnumerable<UserRoleEntity>> GetListAsync(long branchId)
        {
            var tb = await _repository.Find(x => x.BranchId == branchId).ToListAsync();
            return tb.Select(x => x.AsEntity());
        }

        public async Task<UserRoleEntity> UpdateAsync(UserRoleEntity userRoleEntity)
        {
            var tb = await _repository.UpdateAsync(userRoleEntity.AsTable());
            return tb.AsEntity();
        }
    }
}
