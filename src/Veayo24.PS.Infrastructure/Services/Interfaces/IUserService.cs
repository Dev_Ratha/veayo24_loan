﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Dto.Users;

namespace Veayo24.PS.Infrastructure.Services.Interfaces
{
    public interface IUserService
    {
        Task<UserDto> LoginAsync(string userName, string password);

        Task<UserDto> GenerateToken(Guid userId);
    }
}
