﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Veayo24.PS.Core.Dto.Users;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Core.Exceptions.User;
using Veayo24.PS.Core.Repositories.Users;
using Veayo24.PS.Infrastructure.Services.Interfaces;
using Veayo24.PS.Lib.Jwt;

namespace Veayo24.PS.Infrastructure.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly IPasswordHasher<UserEntity> _passwordHasher;
        private readonly ITokenProvider<UserEntity> _tokenProvider;
        private readonly IDataProtector _dataProtector;

        public UserService(
            IUserRepository userRepository,
            IPasswordHasher<UserEntity> passwordHasher,
            ITokenProvider<UserEntity> tokenProvider,
            IDataProtector dataProtector)
        {
            _userRepository = userRepository;
            _passwordHasher = passwordHasher;
            _tokenProvider = tokenProvider;
            _dataProtector = dataProtector;
        }
        public Task<UserDto> GenerateToken(Guid userId)
        {
            throw new NotImplementedException();
        }

        public async Task<UserDto> LoginAsync(string userName, string password)
        {
            var user = await _userRepository.GetByUserNameAsync(userName);
            if (user == null)
                throw new InvalidPasswordException();

            var valid = user.ValidatePassword(password, _passwordHasher);
            if (!valid)
                throw new InvalidPasswordException();
            string token;
            token = user.CreateToken(_tokenProvider);
            user.SetToken(token, _dataProtector);
            await _userRepository.UpdateAsync(user);
            var dto = user.AsDto();
            dto.Token = token;
            return dto;
        }
    }
}
