﻿using System.Globalization;
using AspNetCoreRateLimit;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Localization;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Core.Exceptions;
using Veayo24.PS.Core.Middlewares;
using Veayo24.PS.Infrastructure.Services;
using Veayo24.PS.Infrastructure.Services.Interfaces;
using Veayo24.PS.Infrastructure.SQLServers;
using Veayo24.PS.Infrastructure.Swagger.CustomizeHeaders;
using Veayo24.PS.Infrastructure.Swagger.RequestExamples;
using Veayo24.PS.Lib.CQRS.Commands;
using Veayo24.PS.Lib.CQRS.Events;
using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.Data;
using Veayo24.PS.Lib.DataProtector;
using Veayo24.PS.Lib.EntityFramework;
using Veayo24.PS.Lib.Jwt;
using Veayo24.PS.Lib.Localization;
using Veayo24.PS.Lib.Logging;
using Veayo24.PS.Lib.MediaUpload;
using Veayo24.PS.Lib.RequestRateLimit;
using Veayo24.PS.Lib.Swagger;

namespace Veayo24.PS.Infrastructure
{
    public static class Extentions
    {
        public static IServiceCollection AddInfrastructure(this IServiceCollection services)
        {
            services.AddTransient<IPasswordHasher<UserEntity>, PasswordHasher<UserEntity>>();
            services.AddTransient<ITokenProvider<UserEntity>, TokenProvider<UserEntity>>();

            services.AddJwt();
            services.AddRequestRateLimit();

            services.AddMediaUploadProviderFactory();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddSingleton<ICommandDispatcher, CommandDispatcher>();
            services.AddSingleton<IQueryDispatcher, QueryDispatcher>();
            services.AddSingleton<IEventDispatcher, EventDispatcher>();

            services.AddCommandHandlers();
            services.AddQueryHandlers();
            services.AddEventHandlers();

            //services.AddOneSignal();
            //services.AddTransient<INotificationDb, NotificationDb>();
            //services.AddMailKit();

            services.AddSQLServer<SqlDbContext>();
            services.AddDbContextRepositories();
            services.AddTransient<IDBSeeder, SqlDbSeeder>();
            services.AddTransient<IUserService, UserService>();
            services.AddRepositories();

            services.AddSwagger<AuthorizationHeaderParameterOperationFilter>()
                .AddSwaggerExample();

            services.AddDataProtector();

            //.AddMessageConsumer(new FirstScheduleConsumer());

            services.AddLocalize("Veayo24.PS.Lib.Localization.Resources.lang");
            services.AddLocalization();
            services.Configure<ForwardedHeadersOptions>(options =>
            {
                options.ForwardedHeaders = ForwardedHeaders.All;
            });
            return services;
        }

        public static IApplicationBuilder UseInfrastructure(this IApplicationBuilder app)
        {
            var supportedCultures = new[]
            {
                new CultureInfo("en-US"),
                new CultureInfo("fr")
            };

            app.UseForwardedHeaders();

            app.UseErrorHandler()
                .UseMiddleware<AuthorizationRequestHandlerMiddleware>()
                .UseMiddleware<LogMiddleware>()
                .UseInitializer()
                .UseIpRateLimiting()
                .UseRequestLocalization(new RequestLocalizationOptions
                {
                    DefaultRequestCulture = new RequestCulture("en-US"),
                    // Formatting numbers, dates, etc.
                    SupportedCultures = supportedCultures,
                    // UI strings that we have localized.
                    SupportedUICultures = supportedCultures
                })
                .UseAllForwardedHeaders()
                .UseLogUserIdMiddleware();

            return app;
        }

        public static IApplicationBuilder UseAllForwardedHeaders(this IApplicationBuilder builder)
            => builder.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = ForwardedHeaders.All
            });

        public static IApplicationBuilder UseErrorHandler(this IApplicationBuilder builder)
            => builder.UseMiddleware<ErrorHandlerMiddleware>();

        public static IApplicationBuilder UseInitializer(this IApplicationBuilder builder)
        {
            using var scope = builder.ApplicationServices.CreateScope();
            var sp = scope.ServiceProvider;
            sp.GetRequiredService<IDBInitializer>().InitializeAsync();
            return builder;
        }

        public static IApplicationBuilder UseLogUserIdMiddleware(this IApplicationBuilder builder)
            => builder.UseMiddleware<LogUserIdMiddleware>();
    }
}
