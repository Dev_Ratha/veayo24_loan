﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Application.Queries.UserRoles;
using Veayo24.PS.Core.Dto.UserRoles;
using Veayo24.PS.Core.Repositories;
using Veayo24.PS.Lib.CQRS.Queries;

namespace Veayo24.PS.Infrastructure._QueryHandlers.UserRoles
{
    public class GetUserRoleListQueryHandler : IQueryHandler<GetUserRoleListQuery, IEnumerable<UserRoleDto>>
    {
        private readonly IUserRoleRepository _userRoleRepository;

        public GetUserRoleListQueryHandler(IUserRoleRepository userRoleRepository)
        {
            _userRoleRepository = userRoleRepository;
        }
        public async Task<IEnumerable<UserRoleDto>>? HandleAsync(GetUserRoleListQuery query)
        {
            var entities = await _userRoleRepository.GetListAsync(query.BranchId);
            return entities.Select(x => new UserRoleDto { Id = x.Id, Role = x.Role });
        }
    }
}
