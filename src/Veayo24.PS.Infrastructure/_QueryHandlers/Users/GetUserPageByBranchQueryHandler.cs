﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Application.Queries.Users;
using Veayo24.PS.Core.Dto.Users;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Core.Repositories.Users;
using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Infrastructure._QueryHandlers.Users
{
    public class GetUserPageByBranchQueryHandler : IQueryHandler<GetUserPageByBranchQuery, Pagination<UserDto>>
    {
        private readonly IUserRepository _userRepository;

        public GetUserPageByBranchQueryHandler(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }
        public async Task<Pagination<UserDto>>? HandleAsync(GetUserPageByBranchQuery query)
        {
            var page = await _userRepository.GetUserPageByBranch(query.BranchId, query.PageId, query.Result);
            return page.Map(x => x.AsDto());
        }
    }
}
