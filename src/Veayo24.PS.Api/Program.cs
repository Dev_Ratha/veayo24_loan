﻿using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Hosting;
using Veayo24.PS.Lib.Logging;
using Veayo24.PS.Lib.Vault;

namespace Veayo24.PS.Api
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args)
        {

            return Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder =>
                {
                    webBuilder.UseStartup<Startup>()
                    .ConfigureKestrel((context, options) =>
                    {
                        options.AllowSynchronousIO = true;
                    });
                })
            .UseVault()
            .UseLogging();

        }

    }
}
