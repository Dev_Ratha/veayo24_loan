﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mip.ApiAuthCore.Services.ServiceInterfaces;

namespace Mip.ApiAuth.Controllers
{
    [Route("api/biz")]
    [ApiController]
    [Authorize]
    public class BusinessController: ControllerBase
    {
    }
}
