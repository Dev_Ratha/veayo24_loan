﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Veayo24.PS.Core.Exceptions.User;
using System.Linq;

namespace Veayo24.PS.Api.Controllers
{
    [Route("v{version:apiVersion}/[controller]")]

    [ApiController]
    [Authorize]
    public abstract class BaseController : ControllerBase
    {
        private const string ResourceHeader = "X-Resource";
        private const string IdentityHeader = "X-Identity";


        public BaseController()
        {
        }

        protected ActionResult Accepted(string resource, long resourceId)
        {
            if (!string.IsNullOrWhiteSpace(resourceId.ToString()))
            {
                Response.Headers.Add(ResourceHeader, $"{resource}/{resourceId}");
            }

            return base.Accepted();
        }

        protected ActionResult AcceptedWithResource(string resource, string identity)
        {
            if (!string.IsNullOrWhiteSpace(resource))
            {
                Response.Headers.Add(ResourceHeader, $"{resource}");
                Response.Headers.Add(IdentityHeader, $"{identity}");
            }

            return base.Accepted();
        }

        protected ActionResult OkWithResource(Object obj, string resource, string identity)
        {
            if (!string.IsNullOrWhiteSpace(resource))
            {
                Response.Headers.Add(ResourceHeader, $"{resource}");
                Response.Headers.Add(IdentityHeader, $"{identity}");
            }

            return base.Ok(obj);
        }

        protected long UserId
        {
            get
            {
                var name = User?.Identity?.Name;
                if (string.IsNullOrWhiteSpace(name))
                    return long.MinValue;

                long.TryParse(name, out var id);

                return id;
            }
        }
        protected long? BranchId
        {
            get
            {
                var name = User.Claims.FirstOrDefault(x => x.Type == "x-branch")?.Value.ToLower();
                if (string.IsNullOrWhiteSpace(name))
                    return null;

                long.TryParse(name, out var id);

                return id;
            }
        }
        protected void TryParseId(string id)
        {
            long.TryParse(id, out var validId);
            ValidateUserId(validId);
        }

        protected void TryParseId(string id, out long validId)
        {
            long.TryParse(id, out validId);
            ValidateUserId(validId);
        }

        protected void ValidateUserId(long id)
        {
            if (!id.Equals(UserId))
                throw new InvalidUserIdException(id.ToString());
        }
    }
}
