﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Veayo24.PS.Application.Queries.UserRoles;
using Veayo24.PS.Core.Dto.UserRoles;
using Veayo24.PS.Lib.CQRS.Queries;

namespace Veayo24.PS.Api.Controllers.V1
{
    public class UserRoleController : BaseController
    {
        private readonly IQueryDispatcher _query;

        public UserRoleController(IQueryDispatcher query)
        {
            _query = query;
        }
        [HttpGet("list")]
        public async Task<ActionResult> GetListAsync()
        {
            var q = new GetUserRoleListQuery(BranchId ?? 0);
            var res = await _query.QueryAsync<GetUserRoleListQuery, IEnumerable<UserRoleDto>>(q);
            return Ok(res);
        }
    }
}
