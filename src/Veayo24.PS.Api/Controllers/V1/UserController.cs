﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Veayo24.PS.Application.Queries.Users;
using Veayo24.PS.Core.Dto.Paginations;
using Veayo24.PS.Core.Dto.Users;
using Veayo24.PS.Core.Exceptions.User;
using Veayo24.PS.Infrastructure.Services.Interfaces;
using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.EntityFramework;
using Veayo24.PS.Lib.Jwt;

namespace Veayo24.PS.Api.Controllers.V1
{
    public class UserController : BaseController
    {
        private readonly IUserService _userService;
        private readonly IQueryDispatcher _query;

        public UserController(IUserService userService, IQueryDispatcher query)
        {
            _userService = userService;
            _query = query;
        }

        [AllowAnonymous]
        [HttpPost("login")]
        public async Task<ActionResult<JsonWebToken>> LoginAsync([FromBody] LoginDto dto)
        {
            var user = await _userService.LoginAsync(dto.UserName, dto.Password);
            if (user == null)
                throw new InvalidPasswordException();
            var token = new JsonWebToken { AccessToken = user?.Token ?? string.Empty };
            return OkWithResource(token, $"user/{user?.Id.ToString() ?? string.Empty}", user?.Id.ToString() ?? string.Empty);
        }

        [HttpPost("signup")]
        public async Task<ActionResult> SignUpAsync()
        {
            return Ok();
        }
        [HttpGet("page")]
        public async Task<ActionResult<Pagination<UserDto>>> GetUserByBranch([FromQuery] PaginationDto dto)
        {
            var q = new GetUserPageByBranchQuery(BranchId, dto.PageId ?? 1, dto.Result ?? 10);
            var res = await _query.QueryAsync<GetUserPageByBranchQuery, Pagination<UserDto>>(q);
            return Ok(res);
        }
    }
}
