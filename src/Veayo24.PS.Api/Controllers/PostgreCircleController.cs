﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mip.ApiAuthCore.Models.EntityModels;
using Mip.ApiAuthCore.Models.ViewModels;
using Mip.ApiAuthCore.Services.ServiceInterfaces;
using Mip.ApiAuthCore.Utils;
using Mip.ApiAuthDb;

namespace Mip.ApiAuth.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class PostgreCircleController : ControllerBase
    {
        private readonly AuthDbContext _context;
        private readonly ICircleService _circleService;

        public PostgreCircleController(ICircleService circleService, AuthDbContext context)
        {
            _context = context;
            _circleService = circleService;
        }

        // GET: api/Circle
        [HttpGet]
        public async Task<ActionResult<IEnumerable<CircleListViewModel>>> GetCircles([FromQuery(Name = "page")] int page, [FromQuery(Name = "limit")] int limit)
        {
            int UserId = int.Parse(User.Identity.Name);

            return await _circleService.GetCircleWithPlaceCount(UserId, page, limit);
        }

        // GET: api/Circle/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Circle>> GetCircle(int id)
        {
            var circle = await _context.Circles.FindAsync(id);

            if (circle == null)
            {
                return NotFound();
            }

            return circle;
        }

        [HttpGet("default")]
        public async Task<ActionResult<Circle>> GetDefaultCircle()
        {
            int userId = int.Parse(User.Identity.Name);
            var circle = await _context.Circles.FirstAsync(c => c.UserId == userId);

            if (circle == null)
            {
                return NotFound();
            }

            return circle;
        }

        // PUT: api/Circle/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutCircle(int id, Circle circle)
        {
            int userId = int.Parse(User.Identity.Name);

            if (!AuthHelper.isOwner(userId, circle.UserId))
            {
                return Forbid();
            }

            if (id != circle.Id)
            {
                return BadRequest();
            }

            circle.UserId = userId;
            _context.Entry(circle).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CircleExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Circle
        [HttpPost]
        public async Task<ActionResult<Circle>> PostCircle(Circle circle)
        {
            circle.UserId = int.Parse(User.Identity.Name);

            await _circleService.CreateCircle(circle);

            return CreatedAtAction("GetCircle", new { id = circle.Id }, circle);
        }

        // DELETE: api/Circle/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Circle>> DeleteCircle(int id)
        {
            var circle = await _context.Circles.FindAsync(id);
            if (circle == null)
            {
                return NotFound();
            }

            _context.Circles.Remove(circle);
            await _context.SaveChangesAsync();

            return circle;
        }

        private bool CircleExists(int id)
        {
            return _context.Circles.Any(e => e.Id == id);
        }
    }
}
