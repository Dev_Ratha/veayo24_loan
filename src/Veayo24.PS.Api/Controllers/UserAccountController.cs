﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mip.ApiAuthCore.Exceptions;
using Mip.ApiAuthCore.ServiceInterfaces;
using Mip.ApiAuthCore.Utils;
using Mip.ApiAuthCore.Utils.Constants;
using Mip.ApiAuthCore.Models.ViewModels;
using PhoneNumbers;
using Mip.ApiAuthCore.Models.EntityModels;
using Mip.ApiAuthCore.Services.ServiceInterfaces;

namespace Mip.ApiAuth.Controllers
{
    [Route("ua")]
    [ApiController]
    [Produces("application/json")]
    [Authorize]
    public class UserAccountController : ControllerBase
    {
        private IUserAccountService _userAccountService;
        private ICircleService _circleService;

        public UserAccountController(IUserAccountService userAccountService, ICircleService circleService)
        {
            _userAccountService = userAccountService;
            _circleService = circleService;
        }

        /// <summary>
        /// Register new account
        /// </summary>
        /// <param name="registerViewModel"></param>
        /// <returns></returns>
        /// <response code="409">Phone number already register.</response>
        [Route("reg")]
        [HttpPost]
        [AllowAnonymous]
        [ProducesResponseType(typeof(AuthenticateViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult<AuthenticateViewModel>> Register([FromBody] RegisterViewModel registerViewModel)
        {
            try
            {
                var ua = new UserAccount(registerViewModel);

                var auth = await _userAccountService.RegisterAsync(ua);
                if (auth != null)
                {
                    // Create Default Circle 
                    Circle circle = new Circle() {
                        Name = "Default",
                        UserId = auth.User.Id
                    };

                    await _circleService.CreateCircle(circle);

                    return Ok(auth);
                }

                return Conflict(ErrorMessages.PhoneAlreadyRegistered);
            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        /// <summary>
        /// Login account
        /// </summary>
        /// <param name="loginViewModel"></param>
        /// <returns></returns>
        /// <response code="400">Invalid phone number</response>
        /// <response code="401">Login failed</response>
        [Route("login")]
        [HttpPost]
        [ProducesResponseType(typeof(AuthenticateViewModel), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [AllowAnonymous]
        public async Task<ActionResult<AuthenticateViewModel>> Login([FromBody] LoginViewModel loginViewModel)
        {
            try
            {
                loginViewModel.PhNumber = PhoneHelper.PhoneNumberParser(loginViewModel.PhNumber, loginViewModel.PhRegion);
                var auth = await _userAccountService.Login(loginViewModel);

                if (auth == null) return Unauthorized(ErrorMessages.LoginFailed);

                return Ok(auth);
            }
            catch (InvalidPhoneException)
            {
                return BadRequest(ErrorMessages.InvalidPhone);
            }
        }

        /// <summary>
        /// Change account password
        /// </summary>
        /// <param name="changePasswordViewModel"></param>
        /// <returns></returns>
        /// <response code="403">Incorrect old password</response>
        [Route("chgpwd")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status401Unauthorized)]
        [ProducesResponseType(StatusCodes.Status403Forbidden)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult> ChangePassword([FromBody] ChangePasswordViewModel changePasswordViewModel)
        {
            try
            {
                var claimsIdentity = User.Identity as ClaimsIdentity;
                int userId = Convert.ToInt32(claimsIdentity.FindFirst(ClaimTypes.Name)?.Value);

                await _userAccountService.ChangePasswordAsync(userId, changePasswordViewModel);

                return Ok();
            }
            catch (IncorrectOldPasswordException)
            {
                return Forbid();
            }
            catch (UserAccountNotFoundException)
            {
                return Unauthorized();
            }
        }
    }
}