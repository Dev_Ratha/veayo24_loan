﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Veayo24.PS.Api.Controllers.V2
{
    [ApiVersion("2")]
    public class AppController : BaseController
    {
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult> Get()
        {
            return Ok("Version 2 Okay");
        }
    }
}
