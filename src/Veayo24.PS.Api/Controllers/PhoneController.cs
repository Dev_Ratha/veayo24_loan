﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Mip.ApiAuthCore.Exceptions;
using Mip.ApiAuthCore.ServiceInterfaces;
using Mip.ApiAuthCore.Utils;
using Mip.ApiAuthCore.Utils.Constants;
using Mip.ApiAuthCore.Models.ViewModels;

namespace Mip.ApiAuth.Controllers
{
    [Route("ph")]
    [ApiController]
    public class PhoneController : ControllerBase
    {

        IPhoneService _phoneService;

        public PhoneController(IPhoneService phoneService)
        {
            _phoneService = phoneService;
        }

        /// <summary>
        /// Check if phone number already register.
        /// </summary>
        /// <returns></returns>
        /// <response code="204">Phone number not yet register.</response>
        /// <response code="400">Invalid phone number.</response>
        /// <response code="409">Phone number already register.</response>
        [Route("check")]
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status409Conflict)]
        public async Task<ActionResult> CheckExistedPhone([FromBody] PhoneViewModel phoneViewModel)
        {
            try
            {
                phoneViewModel.PhNumber = PhoneHelper.PhoneNumberParser(phoneViewModel.PhNumber, phoneViewModel.PhRegion);
                var registered = await _phoneService.CheckRegisteredPhoneAsync(phoneViewModel);
                if (!registered) return NoContent();
                else return Conflict(ErrorMessages.PhoneAlreadyRegistered);
            }
            catch (InvalidPhoneException)
            {
                return BadRequest(ErrorMessages.InvalidPhone);
            }
        }
    }
}
