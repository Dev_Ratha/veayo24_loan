using System.ComponentModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Mip.ApiAuthCore.Models.EntityModels;
using Mip.ApiAuthCore.Services.ServiceInterfaces;
using Mip.ApiAuthCore.Utils;
using Mip.ApiAuthDb;

namespace Mip.ApiAuth.Controllers
{
    [Route("api")]
    [ApiController]
    public class CirclePlaceController : ControllerBase
    {
        private readonly AuthDbContext _context;
        private readonly ICircleService _circleService;

        public CirclePlaceController(ICircleService circleService, AuthDbContext context)
        {
            _context = context;
            _circleService = circleService;
        }

        // GET: api/CirclePlace
        [HttpGet("circle/{circleId}/circleplace")]
        public async Task<ActionResult<IEnumerable<CirclePlace>>> GetCirclePlaces(int circleId, [FromQuery(Name="page")] int page = 1, [FromQuery(Name="limit")] int limit = 50)
        {
            return await _circleService.GetCirclePlaces(circleId, page, limit);
        }

        // GET: api/CirclePlace/5
        [HttpGet("circle/{circleId}/circleplace/{id}")]
        public async Task<ActionResult<CirclePlace>> GetCirclePlace(int circleId, int id)
        {
            var circlePlace = await _context.CirclePlaces.FindAsync(id);

            if (circlePlace == null)
            {
                return NotFound();
            }

            return circlePlace;
        }

        // PUT: api/CirclePlace/5
        [HttpPut("circle/{circleId}/circleplace/{id}")]
        public async Task<IActionResult> PutCirclePlace(int circleId, int id, CirclePlace circlePlace)
        {
            int userId = int.Parse(User.Identity.Name);
            Circle circle = await _context.Circles.FindAsync(circleId);
            if (!AuthHelper.isOwner(userId, circle.UserId)) 
            {
                return Forbid();
            }

            if (id != circlePlace.Id)
            {
                return BadRequest();
            }

            if (circle == null) {
                return NotFound();
            }

            circlePlace.Circle = circle;
            _context.Entry(circlePlace).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CirclePlaceExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/CirclePlace
        [HttpPost("circle/{circleId}/circleplace")]
        public async Task<ActionResult<CirclePlace>> PostCirclePlace(int circleId, CirclePlace circlePlace)
        {
            int userId = int.Parse(User.Identity.Name);
            Circle circle = await _context.Circles.FindAsync(circleId);
            if (!AuthHelper.isOwner(userId, circle.UserId)) 
            {
                return Forbid();
            }

            if (circle == null)
            {
                return NotFound();
            }

            circlePlace.Circle = circle;

            _context.CirclePlaces.Add(circlePlace);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetCirclePlace", new { circleId , id = circlePlace.Id }, circlePlace);
        }

        // DELETE: api/CirclePlace/5
        [HttpDelete("circle/{circleId}/circleplace/{id}")]
        public async Task<ActionResult<CirclePlace>> DeleteCirclePlace(int circleId, int id)
        {
            int userId = int.Parse(User.Identity.Name);
            Circle circle = await _context.Circles.FindAsync(circleId);
            if (!AuthHelper.isOwner(userId, circle.UserId)) 
            {
                return Forbid();
            }

            var circlePlace = await _context.CirclePlaces.FindAsync(id);
            if (circlePlace == null)
            {
                return NotFound();
            }

            _context.CirclePlaces.Remove(circlePlace);
            await _context.SaveChangesAsync();

            return circlePlace;
        }

        private bool CirclePlaceExists(int id)
        {
            return _context.CirclePlaces.Any(e => e.Id == id);
        }
    }
}
