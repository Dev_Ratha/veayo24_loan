﻿using System;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using Newtonsoft.Json;
using Veayo24._PS.Client.Exceptions;
using Veayo24._PS.Client.Models.Exceptions;

namespace Veayo24._PS.Client.Utils.HttpClients;

public static class Extensions
{
    public static async Task<T> DeserializeAsync<T>(this HttpResponseMessage response)
    {
        if (!response.IsSuccessStatusCode)
        {
            var str = await response.Content.ReadAsStringAsync();
            var model = JsonConvert.DeserializeObject<ExceptionModel>(str);
            if (model == null)
                throw new ApiException();
            var ex = model.CreateException();
            throw ex;
        }
        else
        {
            return await response.Content.ReadFromJsonAsync<T>();
        }
    }
}
