﻿using System.Threading.Tasks;
using AntDesign;
using Microsoft.AspNetCore.Components;

namespace Veayo24._PS.Client.Components;

public class BaseModal : ComponentBase
{
    [Parameter]
    public bool Loading { get; set; } = false;
    [Parameter]
    public bool Visible { get; set; } = false;
    [Parameter]
    public bool ConfirmLoading { get; set; } = false;
    [Parameter]
    public EventCallback<bool> OnVisibleChanged { get; set; }

    [Parameter]
    public EventCallback<bool> OnLoadingChanged { get; set; }

    public async Task SetLoading(bool val)
    {
        Loading = val;
        await OnLoadingChanged.InvokeAsync(Loading);
        StateHasChanged();
    }
    public async Task SetVisible(bool val)
    {
        Visible = val;
        await OnVisibleChanged.InvokeAsync(val);
        StateHasChanged();
    }

    public async Task OnCancel()
    {
        Visible = false;
        await OnVisibleChanged.InvokeAsync(false);
        await OnClear();
    }
    public virtual async Task OnClear()
    {

    }

}
