﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text.Json;
using System.Threading.Tasks;
using AntDesign;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Forms;
using SchoolClient.Models.Logins;
using Veayo24._PS.Client.Auths;
//using Veayo24.PS.Core.Exceptions.User;

namespace Veayo24._PS.Client.Pages.Logins
{
    public partial class Login
    {
        public bool Loading { get; set; }
        private LoginFormModel model = new LoginFormModel();
        [Inject]
        private AuthenticationStateProvider _authenticationStateProvider { get; set; }
        [Inject]
        private HttpClient _httpClient { get; set; }

        private async Task OnFinish(EditContext editContext)
        {
            Loading = true;
            try
            {
                var res = await _userService.LoginAsync(model);
                await _userService.SaveToken(res.AccessToken);
                ((ApiAuthenticationStateProvider)_authenticationStateProvider).MarkUserAsAuthenticated(model.UserName);
                _httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", res.AccessToken);

                NavigationManager.NavigateTo("/");
            }
            catch
            {
                Loading = false;
            }




            Console.WriteLine($"Success:{JsonSerializer.Serialize(model)}");
        }
    }
}
