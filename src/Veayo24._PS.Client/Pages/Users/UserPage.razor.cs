﻿using AntDesign;
using Microsoft.AspNetCore.Components;
using Veayo24._PS.Client.Models.Pagination;
using Veayo24._PS.Client.Models.Users;
using Veayo24._PS.Client.Resources;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Pages.Users
{
    public partial class UserPage
    {
        public Pagination<UserModel> PageData { get; set; } = Pagination<UserModel>.Empty();
        public PageDto Page { get; set; } = PageDto.Empty();
        public bool ListLoading { get; set; }
        public bool ModalVisible { get; set; }

        //inject
        [Inject] IUserService _userService { get; set; }
        [Inject] ModalService _modalService { get; set; }
        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            await LoadPageAsync();
        }

        private async Task LoadPageAsync()
        {
            try
            {
                ListLoading = true;
                PageData = await _userService.GetUserPage(Page);
                Page.Update(PageData);
                ListLoading = false;
            }
            catch
            {
                ListLoading = false;
            }

        }
        private async Task SearchAsync(string search)
        {
            Page.Search = search;
            await LoadPageAsync();
        }
        private async Task RowRemoveAsync(UserModel user)
        {
            var options = new ConfirmOptions
            {
                Title = AppResource.ModalRemoveTitle,
                Content = AppResource.ModalRemoveContent,
                OkType = "danger",
                OkText = AppResource.ButtonRemove
            };
            await _modalService.ConfirmAsync(options);
        }
    }
}
