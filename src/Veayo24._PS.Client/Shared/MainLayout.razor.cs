﻿
using AntDesign.ProLayout;
using Microsoft.AspNetCore.Components;
using Veayo24._PS.Client.Services;

namespace Veayo24._PS.Client.Shared
{
    public partial class MainLayout : IDisposable
    {
        public bool Loading { get; set; } = true;
        public IEnumerable<MenuDataItem> MenuData { get; set; } = new MenuDataItem[] { };
        //[Inject] public INavMenuService _service { get; set; }

        [Inject] public HttpInterceptorService _interceptor { get; set; }

        public void Dispose()
        {
            _interceptor.DisposeEvent();
        }
    }
}
