﻿using System.Net.Http.Headers;
using System.Net.Http.Json;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components.Authorization;
using SchoolClient.Models.Logins;
using Veayo24._PS.Client.Auths;
using Veayo24._PS.Client.Models.Logins;
using Veayo24._PS.Client.Models.Pagination;
using Veayo24._PS.Client.Models.Users;
using Veayo24._PS.Client.Services.Interfaces;
using Veayo24._PS.Client.Utils.HttpClients;

namespace Veayo24._PS.Client.Services
{
    public class UserService : IUserService
    {
        private readonly HttpClient _http;
        private readonly ILocalStorageService _localStorage;
        private readonly AuthenticationStateProvider _authenticationStateProvider;

        public UserService(HttpClient http, ILocalStorageService localStorage, AuthenticationStateProvider authenticationStateProvider)
        {
            _http = http;
            _localStorage = localStorage;
            _authenticationStateProvider = authenticationStateProvider;
        }
        public async Task CheckUserTokenAsync()
        {
            var token = await GetToken();
            var provider = ((ApiAuthenticationStateProvider)_authenticationStateProvider);
            if (token == null)
            {
                provider.MarkUserAsLoggedOut();
                _http.DefaultRequestHeaders.Authorization = null;
            }
            else
            {
                _http.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("bearer", token);
                provider.MarkUserAsAuthenticated("general");
            }
        }

        public async Task ClearToken()
        {
            await _localStorage.SetItemAsStringAsync("token", string.Empty);
        }

        public async Task<string> GetToken()
        {
            var t = await _localStorage.GetItemAsStringAsync("token");
            return t;
        }

        public async Task<Pagination<UserModel>> GetUserPage(PageDto page)
        {
            var res = await _http.GetFromJsonAsync<Pagination<UserModel>>($"/v1/user/page?{page.GetUrl()}");
            return res;
        }

        public async Task<JsonWebToken> LoginAsync(LoginFormModel model)
        {

            var res = await _http.PostAsJsonAsync($"/v1/user/login", model);
            var dto = await res.DeserializeAsync<JsonWebToken>();
            return dto;
        }

        public async Task SaveToken(string token)
        {
            await _localStorage.SetItemAsStringAsync("token", token);
        }
    }
}
