﻿using SchoolClient.Models.Logins;
using SchoolClient.Services.Interfaces;
using Veayo24._PS.Client.Models.Logins;
using Veayo24._PS.Client.Models.Pagination;
using Veayo24._PS.Client.Models.Users;

namespace Veayo24._PS.Client.Services.Interfaces
{
    public interface IUserService : IService
    {
        Task<JsonWebToken> LoginAsync(LoginFormModel model);
        Task SaveToken(string token);
        Task ClearToken();
        Task<string> GetToken();
        Task CheckUserTokenAsync();
        Task<Pagination<UserModel>> GetUserPage(PageDto page);
    }
}
