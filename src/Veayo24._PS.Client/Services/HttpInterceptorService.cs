﻿using System;
using System.Net;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;
using AntDesign;
using Blazored.LocalStorage;
using Microsoft.AspNetCore.Components;
using Toolbelt.Blazor;

namespace Veayo24._PS.Client.Services;

public class HttpInterceptorService
{
    private readonly HttpClientInterceptor _interceptor;
    private readonly ModalService _modalService;
    private readonly NavigationManager _navigation;

    public HttpInterceptorService(HttpClientInterceptor interceptor, ModalService modalService, NavigationManager navigation)
    {
        _interceptor = interceptor;
        _modalService = modalService;
        _navigation = navigation;
    }

    public void MonitorEvent() => _interceptor.AfterSendAsync += InterceptResponse;

    private async Task InterceptResponse(object sender, HttpClientInterceptorEventArgs e)
    {
        if (e?.Response == null || e.Response.IsSuccessStatusCode) return;

        var responseCode = e.Response.StatusCode;
        var re = await e.Response.Content.ReadAsStringAsync();
        Console.WriteLine(re);
        Console.WriteLine(e.Response.StatusCode);
        var message = "Something went wrong, please contact Administrator";
        string detail;

        switch (responseCode)
        {
            case HttpStatusCode.NotFound:
                break;
            case HttpStatusCode.BadRequest:
                showModal("Error", "Opp, an error has accurred!");
                break;
            case HttpStatusCode.Unauthorized:
                showModalUnAuth("Unauthorized", "Login required!");
                break;
            case HttpStatusCode.Forbidden:

                break;
            case HttpStatusCode.InternalServerError:

                break;
            default:
                showModal("Server Problem", "Server has problem please try again later.");
                break;
        }

        throw new HttpRequestException(message);
    }
    private void showModal(string title, string content)
    {
        var option = new ConfirmOptions
        {
            Title = title,
            Content = content,
        };
        _modalService.Error(option);
    }
    private void showModalUnAuth(string title, string content)
    {
        var option = new ConfirmOptions
        {
            Title = title,
            Content = content,
            OnOk = e =>
           {
               _navigation.NavigateTo("/login");
               return Task.CompletedTask;
           }
        };
        _modalService.Error(option);
    }
    //private static async Task<ApiError> GetApiError500FromContent(HttpClientInterceptorEventArgs e)
    //{
    //    try
    //    {
    //        var capturedContent = await e.GetCapturedContentAsync();
    //        var apiError = await capturedContent.ReadFromJsonAsync<ApiError>();
    //        if (apiError != null && !string.IsNullOrEmpty(apiError.Id))
    //        {
    //            return apiError;
    //        }
    //    }
    //    catch
    //    {
    //    }

    //    return null;
    //}

    public void DisposeEvent() => _interceptor.AfterSendAsync -= InterceptResponse;
}
