﻿using System.Net.Http.Json;
using Veayo24._PS.Client.Models.UserRoles;
using Veayo24._PS.Client.Services.Interfaces;

namespace Veayo24._PS.Client.Services
{
    public class UserRoleService : IUserRoleService
    {
        private readonly HttpClient _http;

        public UserRoleService(HttpClient http)
        {
            _http = http;
        }
        public async Task<IEnumerable<UserRoleModel>> GetListAsync()
        {
            var list = await _http.GetFromJsonAsync<IEnumerable<UserRoleModel>>($"/v1/userrole/list");
            return list!;
        }
    }
}
