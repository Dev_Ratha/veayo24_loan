﻿using System.ComponentModel.DataAnnotations;

namespace Veayo24._PS.Client.Models.Users
{
    public class UserModel
    {
        public long? Id { get; set; }
        [Required]
        public string FullName { get; set; }
        [Required]
        public string UserName { get; set; }
        [Required]
        public string Password { get; set; }
        [Required(ErrorMessage = "The User Role field required.")]
        public long? UserRoleId { get; set; }
        public string Role { get; set; }
        public long? MediaId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long? BranchId { get; set; }
        public string Token { get; set; }
    }
}
