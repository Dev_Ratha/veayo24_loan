﻿using Veayo24._PS.Client.Constants;
using Veayo24._PS.Client.Exceptions;
using Veayo24._PS.Client.Exceptions.Users;

namespace Veayo24._PS.Client.Models.Exceptions;

public class ExceptionModel
{
    public string Code { get; set; }
    public string Message { get; set; }

    public ExceptionModel(string code, string message)
    {
        Code = code;
        Message = message;
    }
    public BaseException CreateException()
    {
        switch (Code)
        {
            case ExceptionCode.Default:
                return new ApiException();
            case ExceptionCode.LoginFailed:
                return new LoginFailedException();
        }
        return new ApiException();
    }
}
