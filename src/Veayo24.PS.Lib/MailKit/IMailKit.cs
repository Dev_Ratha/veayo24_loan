using MimeKit;

namespace Veayo24.PS.Lib.MailKit
{
    public interface IMailKit
    {
        void Send(MailboxAddress from, MailboxAddress to, string subject, BodyBuilder body);
    }
}
