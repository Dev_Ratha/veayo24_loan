using System;
using MailKit.Net.Smtp;
using Microsoft.Extensions.DependencyInjection;
using MimeKit;

namespace Veayo24.PS.Lib.MailKit
{
    public class MailKit : IMailKit
    {
        private readonly IServiceProvider _serviceProvider;
        private SmtpClient _client;
        private MailKitOptions _options;
        public MailKit(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _options = _serviceProvider.GetService<MailKitOptions>();
        }

        public void Send(MailboxAddress from, MailboxAddress to, string subject, BodyBuilder body)
        {
            var message = new MimeMessage();

            message.From.Add(from);
            message.To.Add(to);
            message.Subject = subject;
            message.Body = body.ToMessageBody();

            _client = new SmtpClient();
            _client.Connect(_options.Host, _options.Port, true);
            _client.Authenticate(_options.Username, _options.Password);
            _client.Send(message);
            _client.Disconnect(true);
        }
    }
}
