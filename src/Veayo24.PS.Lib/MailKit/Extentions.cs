using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.MailKit
{
    public static class Extentions
    {
        private const string SectionName = "SmtpConfig";
        public static IServiceCollection AddMailKit(this IServiceCollection services)
        {
            var opts = services.GetOptions<MailKitOptions>(SectionName);
            services.AddSingleton(opts);
            services.AddTransient<IMailKit, MailKit>();
            return services;
        }
    }
}
