﻿namespace Veayo24.PS.Lib.MailKit
{
    public class MailKitOptions
    {
        public int Port { get; set; }
        public string Host { get; set; } = string.Empty;
        public string Username { get; set; } = string.Empty;
        public string Password { get; set; } = string.Empty;
    }
}
