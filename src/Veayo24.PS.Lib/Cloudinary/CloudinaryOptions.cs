﻿namespace Veayo24.PS.Lib.Cloudinary
{
    public class CloudinaryOptions
    {
        public string CloudName { get; set; } = string.Empty;
        public string APIKey { get; set; } = string.Empty;
        public string APISecret { get; set; } = string.Empty;
        public string NoProfileId { get; set; } = string.Empty;
        public string Folder { get; set; } = "media";
    }
}
