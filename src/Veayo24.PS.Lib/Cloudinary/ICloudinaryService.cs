﻿using System.Threading.Tasks;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;

namespace Veayo24.PS.Lib.Cloudinary
{
    public interface ICloudinaryService
    {
        ImageUploadResult Upload(string fileName);
        Task<ImageUploadResult?> Upload(IFormFile file);
        GetResourceResult GetImage(string publicId);
        string GetImageUrl(string publicId);
        string GetThumbImageUrl(string publicId);
    }
}
