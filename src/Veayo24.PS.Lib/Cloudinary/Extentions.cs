﻿using System;
using CloudinaryDotNet;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.Cloudinary
{
    public static class Extentions
    {
        private const string SectionName = "Cloudinary";
        public static IServiceCollection AddCloudinary(this IServiceCollection services)
        {
            var options = services.GetOptions<CloudinaryOptions>(SectionName);
            services.AddSingleton(options);

            var account = new Account(
                options.CloudName,
                options.APIKey,
                options.APISecret
            );

            var cloudinary = new CloudinaryDotNet.Cloudinary(account);
            services.AddSingleton(cloudinary);
            services.AddTransient<ICloudinaryService, CloudinaryService>();

            return services;
        }

        public static CloudinaryOptions GetCloudinaryOption(this IServiceProvider services)
        {
            var serviceProvider = services;
            var configuration = serviceProvider.GetService<IConfiguration>();
            return configuration.GetOptions<CloudinaryOptions>(SectionName);
        }

    }
}
