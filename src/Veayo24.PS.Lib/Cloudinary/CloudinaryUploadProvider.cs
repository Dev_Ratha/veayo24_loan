﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Lib.MediaUpload;

namespace Veayo24.PS.Lib.Cloudinary
{
    public class CloudinaryUploadProvider : IMediaUploadProvider
    {
        private readonly IServiceProvider _serviceProvider;
        private readonly CloudinaryOptions _options;

        public CloudinaryUploadProvider(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
            _options = _serviceProvider.GetService<CloudinaryOptions>();
        }

        public string Name => MediaUploadProvider.Cloudinary;

        public string CombineImage(IEnumerable<string> publicIds)
        {

            var bSize = 10;
            if (publicIds.Count() == 1) bSize = 0;
            var firstId = publicIds.ElementAt(0);

            if (firstId == string.Empty && publicIds.Count() == 1)
                return string.Empty;

            if (firstId == string.Empty)
                firstId = NoProfilePublicId();
            var size = 100;
            var imageSize = size - 10;
            var halfSize = size / 2;

            var totalItem = publicIds.Count();
            var cols = (int)Math.Ceiling((decimal)Math.Sqrt(totalItem));

            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();

            var t = new Transformation().Width(imageSize).Height(imageSize).Background("rgb:dedede").Crop("fill").Radius(8).Border($"{bSize}px_solid_rgb:ffffff00").Chain();
            var x = size;
            var y = 0;
            var idx = 0;
            var v = 0;
            var tmp = totalItem % cols;
            for (int r = 0; r < cols; r++)
            {
                var skip = false;
                for (int c = 0; c < cols; c++)
                {
                    if (skip)
                        continue;
                    var te = ((cols - tmp) / 2.0);
                    var con = Math.Ceiling((cols - tmp) / 2.0);
                    if (tmp > 0 && r == 1 && (c + 1) == cols && con > 1)
                    {
                        var value = (int)Math.Floor(te);
                        v -= (halfSize / 2) * value;
                    }

                    if (r == 0 && c == 0)
                    {
                        x = size;
                        y = 0;
                    }
                    else if (r == 0 && c != 0)
                    {
                        x += halfSize;
                    }
                    else if (r > 0 && c == 0)
                    {
                        x = halfSize * (1 - cols);
                        y = halfSize * (r + 1);
                    }
                    else
                    {
                        x += size + v;
                        y = (halfSize * (r + 1)) - halfSize;
                        v = 0;
                    }

                    if (tmp > 0 && ((r + cols) == cols) && (tmp - 1) <= c)
                    {
                        x = (size * c) - halfSize;
                        y = size * (r + 1);
                        v = (cols - tmp) * (halfSize / 2);
                        skip = true;
                        continue;
                    }

                    if ((c + 1) != cols || r != 0)
                    {
                        idx++;
                        if (totalItem > idx)
                        {
                            var publicId = publicIds.ElementAt(idx);
                            if (publicId == string.Empty)
                                publicId = NoProfilePublicId();

                            t.Overlay(new Layer().PublicId(publicId)).Width(imageSize).Height(imageSize).X(x).Y(y).Crop("fill").Radius(8).Border($"{bSize}px_solid_rgb:ffffff00").Chain();
                        }
                        else
                        {
                            t.Overlay(new TextLayer().FontFamily("Times").FontSize(20).Text(" ")).Width(imageSize).Height(size * (r + 1)).X(x).Y(0).Crop("fill").Radius(8).Chain();
                            break;
                        }
                    }
                }
            }
            return cloudinary.Api.UrlImgUp.Transform(t).BuildUrl(firstId);
        }

        public string GetImageThumbUrl(string publicId)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.Api.UrlImgUp.Transform(new Transformation().Quality("auto")).BuildUrl(publicId);
        }


        public string GetImageThumbUrl(string publicId, int height)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.Api.UrlImgUp.Transform(new Transformation().Height(height).Crop("scale").Quality("auto")).BuildUrl(publicId);
        }

        public string GetImageUrl(string publicId)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.Api.UrlImgUp.Secure(true).BuildUrl(publicId);
        }

        public async Task<MediaInfo?> UploadAsync(IFormFile file)
        {
            if (file is null)
                return null;

            var filePath = Path.GetTempFileName();

            if (file.Length > 0)
            {
                using (var stream = new FileStream(filePath, FileMode.Create))
                {
                    using (var image = Image.FromStream(file.OpenReadStream()))
                    {
                        if (image.RawFormat.Guid == ImageFormat.Png.Guid)
                        {
                            using var b = new Bitmap(image.Width, image.Height);
                            b.SetResolution(image.HorizontalResolution, image.VerticalResolution);
                            using (var g = Graphics.FromImage(b))
                            {
                                g.Clear(Color.White);
                                g.DrawImageUnscaled(image, 0, 0);
                            }
                            b.Save(stream: stream, ImageFormat.Jpeg);
                        }
                        else
                        {
                            image.Save(stream, ImageFormat.Jpeg);
                        }
                    }
                }
            }

            return await UploadAsync(filePath);
        }

        public async Task<MediaInfo?> UploadAsync(string fileName)
            => await Task.Run(() =>
            {
                var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();

                var uploadParams = new ImageUploadParams()
                {
                    File = new FileDescription(@fileName),
                    Folder = _options.Folder
                };

                var result = cloudinary.Upload(uploadParams);
                File.Delete(fileName);

                return result.StatusCode == HttpStatusCode.BadRequest ? null : new MediaInfo(result.PublicId, GetImageUrl(result.PublicId), result.Format, result.ResourceType);

            });

        public string NoProfilePublicId()
        {
            var option = _serviceProvider.GetCloudinaryOption();
            return option.NoProfileId;
        }
    }
}
