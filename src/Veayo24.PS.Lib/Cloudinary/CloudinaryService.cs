﻿using System;
using System.IO;
using System.Threading.Tasks;
using CloudinaryDotNet;
using CloudinaryDotNet.Actions;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.Cloudinary
{
    public class CloudinaryService : ICloudinaryService
    {
        private readonly IServiceProvider _serviceProvider;

        public CloudinaryService(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public GetResourceResult GetImage(string publicId)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.GetResource(publicId);
        }

        public string GetImageUrl(string publicId)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.Api.Url.BuildUrl(publicId);
        }

        public string GetThumbImageUrl(string publicId)
        {

            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();
            return cloudinary.Api.UrlImgUp.Transform(new Transformation().Height(200).Crop("scale").Quality("auto:low")).BuildUrl(publicId);

        }

        public ImageUploadResult Upload(string fileName)
        {
            var cloudinary = _serviceProvider.GetService<CloudinaryDotNet.Cloudinary>();

            var uploadParams = new ImageUploadParams()
            {
                File = new FileDescription(@fileName)
            };

            return cloudinary.Upload(uploadParams);
        }

        public async Task<ImageUploadResult?> Upload(IFormFile file)
        {
            if (file is null)
                return null;

            var filePath = Path.GetTempFileName();
            var result = new ImageUploadResult();

            if (file.Length > 0)
            {
                using var stream = new FileStream(filePath, FileMode.Create);
                await file.CopyToAsync(stream);
            }

            return Upload(filePath);
        }
    }
}
