﻿using System;
using Veayo24.PS.Lib.Cloudinary;

namespace Veayo24.PS.Lib.MediaUpload
{
    public class MediaUploadProviderFactory : IMediaUploadProviderFactory
    {
        private readonly IServiceProvider _serviceProvider;

        public MediaUploadProviderFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        public IMediaUploadProvider GetProvider(string providerName)
        {
            switch (providerName)
            {
                case MediaUploadProvider.Cloudinary:
                    return new CloudinaryUploadProvider(_serviceProvider);
                default:
                    throw new MediaUploadProviderNotFoundException();
            }
        }
    }
}
