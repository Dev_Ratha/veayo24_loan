﻿using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.MediaUpload
{
    public static class Extensions
    {
        public static IServiceCollection AddMediaUploadProviderFactory(this IServiceCollection services)
        {
            services.AddTransient<IMediaUploadProviderFactory, MediaUploadProviderFactory>();
            return services;
        }
    }

    public static class MediaUploadProvider
    {
        public const string Cloudinary = "cloudinary";
    }

    //public static class MediaUploadType
    //{
    //    public static string Image = "img";
    //    public static string GIF = "gif";
    //    public static string Video = "vid";
    //}
}
