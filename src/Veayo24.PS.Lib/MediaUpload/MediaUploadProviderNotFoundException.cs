﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Lib.MediaUpload
{
    public class MediaUploadProviderNotFoundException : BaseException
    {
        public override string Code => "media_upload_not_found";
    }
}
