﻿namespace Veayo24.PS.Lib.MediaUpload
{
    public interface IMediaUploadProviderFactory
    {
        IMediaUploadProvider GetProvider(string providerName);
    }
}
