﻿namespace Veayo24.PS.Lib.MediaUpload
{
    public class MediaInfo
    {
        public string PublicId { get; private set; }
        public string Url { get; private set; }
        public string Format { get; private set; }
        public string MediaType { get; private set; }

        public MediaInfo(string publicId, string url, string format, string mediaType)
        {
            PublicId = publicId;
            Url = url;
            Format = format;
            MediaType = mediaType;
        }
    }
}
