﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace Veayo24.PS.Lib.MediaUpload
{
    public interface IMediaUploadProvider
    {
        string Name { get; }
        string GetImageUrl(string publicId);
        string GetImageThumbUrl(string publicId);
        string GetImageThumbUrl(string publicId, int height);
        Task<MediaInfo?> UploadAsync(IFormFile file);
        Task<MediaInfo?> UploadAsync(string fileName);
        string CombineImage(IEnumerable<string> publicIds);
    }
}
