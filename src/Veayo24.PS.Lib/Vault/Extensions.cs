﻿using System;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Configuration.Memory;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Newtonsoft.Json.Linq;

namespace Veayo24.PS.Lib.Vault
{
    public static class Extensions
    {
        public static IHostBuilder UseVault(this IHostBuilder builder)
            => builder.ConfigureServices(services =>
            {
                var options = services.GetOptions<VaultOptions>("Vault");
                if (!options.Enabled) return;

                services.AddSingleton(options);
                services.AddTransient<IVaultStore, VaultStore>();
            })
            .ConfigureAppConfiguration((ctx, cfg) =>
            {
                var options = cfg.Build().GetOptions<VaultOptions>("vault");
                var enabled = options.Enabled;
                var vaultEnabled = Environment.GetEnvironmentVariable("VAULT_ENABLED")?.ToLowerInvariant();
                if (!string.IsNullOrWhiteSpace(vaultEnabled))
                {
                    enabled = vaultEnabled == "true" || vaultEnabled == "1";
                }

                if (!enabled)
                {
                    return;
                }

                Console.WriteLine($"Loading settings from Vault: {options.Url}");
                cfg.AddVault(options);
            });

        private static void AddVault(this IConfigurationBuilder builder, VaultOptions options)
        {
            var client = new VaultStore(options);
            var secret = client.GetDefaultAsync().GetAwaiter().GetResult();
            var parser = new JsonParser();
            var data = parser.Parse(JObject.FromObject(secret));
            var source = new MemoryConfigurationSource { InitialData = data };
            builder.Add(source);
        }
    }
}
