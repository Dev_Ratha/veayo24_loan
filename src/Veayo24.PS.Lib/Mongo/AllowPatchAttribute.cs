﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Mongo
{
    public class AllowPatchAttribute : Attribute
    {
        public bool UpdateNull { get; set; } = false;
    }
}
