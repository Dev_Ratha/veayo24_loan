﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Lib.Types;
using MongoDB.Bson;
using MongoDB.Driver;

namespace Veayo24.PS.Lib.Mongo
{
    public static class Extentions
    {
        private const string SectionName = "Mongo";
        public static IServiceCollection AddMongo(this IServiceCollection services)
        {

            var mongoOptions = services.GetOptions<MongoDbOptions>(SectionName);
            services.AddSingleton(mongoOptions);

            services.AddSingleton(sp =>
            {
                var options = sp.GetRequiredService<MongoDbOptions>();
                return new MongoClient(options.ConnectionString);
            });

            services.AddTransient(sp =>
            {
                var options = sp.GetRequiredService<MongoDbOptions>();
                var client = sp.GetRequiredService<MongoClient>();
                return client.GetDatabase(options.Database);
            });

            services.AddTransient<IMongoDbInitializer, MongoDbInitializer>();
            services.AddTransient<IMongoDbSeeder, MongoDbSeeder>();
            return services;
        }


        public static IServiceCollection AddMongoRepository<TEntity, TIdentifiable>(this IServiceCollection services, string collectionName) where TEntity : IIdentifiable<TIdentifiable>
        {
            services.AddTransient<IMongoRepository<TEntity, TIdentifiable>>(sp =>
            {
                var database = sp.GetRequiredService<IMongoDatabase>();
                return new MongoRepository<TEntity, TIdentifiable>(database, collectionName);
            });

            return services;
        }
        public static ObjectId ToObjectId(this string? id)
        {
            var stringId = id ?? string.Empty;
            ObjectId.TryParse(stringId, out var parsedId);
            return parsedId;
        }

        //public static ObjectId ToObjectId(this string id)
        //{
        //    ObjectId.TryParse(id, out var parsedId);
        //    return parsedId;
        //}

        public static DateTime StartOfDayUTC(this DateTime dateTime)
            => DateTime.SpecifyKind(dateTime.Date, DateTimeKind.Utc);
        public static DateTime EndOfDayUTC(this DateTime dateTime)
            => DateTime.SpecifyKind(dateTime.Date.AddDays(1).AddTicks(-1), DateTimeKind.Utc);

        //public static T ApplyPatchTo<T>(this T patch,T applyPatch) where T : IIdentifiable<ObjectId>
        //{
        //    var dOld = patch.GetDictionaryPropValue();
        //    var dNew = applyPatch.GetDictionaryPropValue();

        //    foreach(var xKey in dOld.Keys)
        //    {
        //        foreach(var yKey in dNew.Keys)
        //        {
        //            if(xKey == yKey)
        //            {

        //            }
        //        }
        //    }

        //    return applyPatch;
        //}

        public static UpdateDefinition<T>? GetUpdateBuilder<T>(this T oldObj) where T : IIdentifiable<ObjectId>
        {
            var b = Builders<T>.Update;
            UpdateDefinition<T>? updateDefination = null;
            var dOld = oldObj.GetDictionaryPropValue();
            foreach (var key in dOld.Keys)
            {
                if (updateDefination is null)
                    updateDefination = b.Set(key, dOld[key]);
                else
                    updateDefination = updateDefination.Set(key, dOld[key]);
            }
            return updateDefination;
        }

        public static Dictionary<string, object?> GetDictionaryPropValue<T>(this T patch) where T : IIdentifiable<ObjectId>
        {
            var d = new Dictionary<string, object?>();
            if (patch is null)
                return d;

            foreach (PropertyInfo propertyInfo in patch.GetType().GetProperties())
            {
                string propertyName = propertyInfo.Name;

                object[] attribute = propertyInfo.GetCustomAttributes(typeof(AllowPatchAttribute), true);
                if (attribute.Length > 0)
                {
                    AllowPatchAttribute attr = (AllowPatchAttribute)attribute[0];
                    var val = propertyInfo.GetValue(patch);
                    if (val != null || attr.UpdateNull)
                        d.Add(propertyName, val);
                }
            }
            return d;
        }

    }
}
