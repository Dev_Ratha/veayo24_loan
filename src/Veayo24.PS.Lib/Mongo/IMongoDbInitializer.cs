﻿using Veayo24.PS.Lib.Types;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Mongo
{
    public interface IMongoDbInitializer
    {
        Task InitializeAsync();
    }
}
