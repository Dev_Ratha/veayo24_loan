﻿using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.Types;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Mongo
{
    public interface IMongoRepository<TEntity, in TIdentifiable> where TEntity : IIdentifiable<TIdentifiable>
    {
        IMongoCollection<TEntity> Collection { get; }
        Task<TEntity> GetAsync(TIdentifiable id);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate);
        Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate);

        Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate,
            TQuery query) where TQuery : IPagedQuery;
        Task<PagedResult<TEntity>> BrowseDescAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> order,
            TQuery query) where TQuery : IPagedQuery;
        Task AddAsync(TEntity entity);
        Task AddManyUnOrderAsync(IEnumerable<TEntity> entities);
        Task UpdateAsync(TEntity entity);
        Task DeleteAsync(TIdentifiable id);
        Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate);

    }
}
