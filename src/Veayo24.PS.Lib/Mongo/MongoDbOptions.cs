﻿namespace Veayo24.PS.Lib.Mongo
{
    public class MongoDbOptions
    {
        public string ConnectionString { get; set; } = string.Empty;
        public string Database { get; set; } = string.Empty;
        public bool Seed { get; set; }
    }
}
