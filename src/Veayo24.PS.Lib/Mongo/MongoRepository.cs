﻿using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.Types;
using MongoDB.Driver;
using MongoDB.Driver.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Mongo
{
    internal class MongoRepository<TEntity, TIdentifiable> : IMongoRepository<TEntity, TIdentifiable>
        where TEntity : IIdentifiable<TIdentifiable>
    {
        public MongoRepository(IMongoDatabase database, string collectionName)
        {
            Collection = database.GetCollection<TEntity>(collectionName);
        }

        public IMongoCollection<TEntity> Collection { get; }

        public async Task<TEntity> GetAsync(TIdentifiable id)
            => await GetAsync(e => e.Id!.Equals(id));

        public async Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> predicate)
            => await Collection.Find(predicate).SingleOrDefaultAsync();

        public async Task<IEnumerable<TEntity>> FindAsync(Expression<Func<TEntity, bool>> predicate)
            => await Collection.Find(predicate).ToListAsync();

        public async Task<PagedResult<TEntity>> BrowseAsync<TQuery>(Expression<Func<TEntity, bool>> predicate,
            TQuery query) where TQuery : IPagedQuery
            => await Collection.AsQueryable().Where(predicate).PaginateAsync(query);

        public async Task AddAsync(TEntity entity)
            => await Collection.InsertOneAsync(entity);
        public async Task AddManyUnOrderAsync(IEnumerable<TEntity> entities)
        {
            if (entities.Count() > 0)
            {
                var _options = new InsertManyOptions() { IsOrdered = false };
                await Collection.InsertManyAsync(entities, _options);
            }
        }
        public async Task UpdateAsync(TEntity entity)
            => await Collection.ReplaceOneAsync(e => e.Id!.Equals(entity.Id), entity);

        public async Task DeleteAsync(TIdentifiable id)
            => await Collection.DeleteOneAsync(e => e.Id!.Equals(id));

        public async Task<bool> ExistsAsync(Expression<Func<TEntity, bool>> predicate)
            => await Collection.Find(predicate).AnyAsync();

        public async Task<PagedResult<TEntity>> BrowseDescAsync<TQuery>(Expression<Func<TEntity, bool>> predicate, Expression<Func<TEntity, object>> order, TQuery query) where TQuery : IPagedQuery
        => await Collection.AsQueryable().Where(predicate).OrderByDescending(order).PaginateAsync(query);
    }
}
