﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Veayo24.PS.Lib.OneSignal
{
    public class OneSignal : IOneSignal
    {
        private readonly IHttpClientFactory _clientFactory;
        private readonly IServiceProvider _serviceProvider;
        private readonly INotificationDb _db;

        public OneSignal(IHttpClientFactory clientFactory, IServiceProvider serviceProvider, INotificationDb db)
        {
            _clientFactory = clientFactory;
            _serviceProvider = serviceProvider;
            _db = db;
        }

        public async Task PushAsync<T>(T data, string userId, bool saveDB = false, TimeSpan delay = default) where T : ONotificationBase
        {
            var options = _serviceProvider.GetService<OneSignalOptions>();
            var notification = new ONotification(options, data, userId);
            var client = _clientFactory.CreateClient("OneSignal");
            var response = await client.PostAsync("", GetJsonPayload(notification));
            await response.Content.ReadAsStringAsync();

            // save to db
            if (saveDB)
            {
                var handler = _serviceProvider.GetService<INotificationDb>();
                if (handler is { })
                    _ = _db.SaveAsync(userId, data);
            }
        }

        protected static StringContent GetJsonPayload(object data)
            => data is null
                ? new StringContent("{}", Encoding.UTF8, "application/json")
                : new StringContent(JsonConvert.SerializeObject(data, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }),
                    Encoding.UTF8, "application/json");
    }
}
