﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Veayo24.PS.Lib.OneSignal
{
    public class NotificationKey : IEquatable<NotificationKey>
    {
        public string Key { get; set; }
        public DateTime CreatedAt { get; set; }

        public NotificationKey(string key, DateTime createdAt)
        {
            Key = key;
            CreatedAt = createdAt;
        }

        public bool Equals([AllowNull] NotificationKey other)
        {
            if (ReferenceEquals(null, other)) return false;
            return ReferenceEquals(this, other) || Key.Equals(other.Key);
        }

        public override int GetHashCode()
        {
            return Key.GetHashCode();
        }

        public static string GenerateKey(string audienceId)
            => $"_noti_{audienceId}";
    }
}
