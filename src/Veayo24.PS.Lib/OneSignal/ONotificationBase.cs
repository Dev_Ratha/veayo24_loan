﻿using System.Collections.Generic;

namespace Veayo24.PS.Lib.OneSignal
{
    public abstract class ONotificationBase
    {
        public abstract string Code { get; }
        public abstract string GenCode { get; }
        public abstract int Version { get; }
        public IEnumerable<NotificationSubject> Subjects { get; set; }
        public IEnumerable<object> Objects { get; private set; }
        public object? headings { get; set; }
        public object contents { get; set; }
        public object[]? buttons { get; set; }
        public Payload payload { get; private set; }
        public string large_icon { get; set; } = string.Empty;
        public bool IsPopup { get; set; } = false;
        public ONotificationBase(IEnumerable<NotificationSubject> subjects, object @object) : this(subjects, new object[] { @object }, @object) { }
        public ONotificationBase(IEnumerable<NotificationSubject> subjects, IEnumerable<object> objects, object data)
        {
            payload = new Payload(data, Code);
            contents ??= new { en = "" };
            Subjects = subjects;
            Objects = objects;
        }

        public abstract IEnumerable<string> GetMsgParams();
        //public abstract string GetResourceKey();
    }
}
