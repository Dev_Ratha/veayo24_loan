﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.OneSignal
{
    public interface INotificationDb
    {
        Task SaveAsync(string userId, ONotificationBase noti);
        Task SaveAsync(string userId, INotification noti, string notificationId = "");
    }
}
