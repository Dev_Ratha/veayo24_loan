﻿using System;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace Veayo24.PS.Lib.OneSignal
{
    public interface INotificationPush
    {
        string android_channel_id { get; set; }
        string app_id { get; set; }
        object[] buttons { get; set; }
        object contents { get; set; }
        Payload data { get; set; }
        object[] filters { get; set; }
        object headings { get; set; }
        string large_icon { get; set; }
        string small_icon { get; set; }
        public string[] include_player_ids { get; set; }
        [JsonIgnore]
        string AudienceId { get; set; }
        StringContent GetJsonPayload();
        Task PushQueueAsync(IServiceProvider sp);
    }
}
