﻿namespace Veayo24.PS.Lib.OneSignal
{
    public class OneSignalOptions
    {
        public string AppId { get; set; } = string.Empty;
        public string ApiKey { get; set; } = string.Empty;
        public string URL { get; set; } = string.Empty;
        public string PopupChannelId { get; set; } = string.Empty;
    }
}
