﻿namespace Veayo24.PS.Lib.OneSignal
{
    public class ONotification
    {
        private readonly OneSignalOptions _options;

        public string app_id { get => _options.AppId; }
        public object[] filters { get; private set; }
        public Payload data { get; private set; }
        public object contents { get; private set; }
        public object? headings { get; private set; }
        public string small_icon { get => "ic_launcher"; }
        public string? large_icon { get; private set; }
        public object[]? buttons { get; private set; }
        public string? android_channel_id { get; set; } = string.Empty;

        public ONotification(OneSignalOptions options, ONotificationBase notification, string userId)
        {
            _options = options;
            data = notification.payload;
            filters = new object[] { new { field = "tag", key = "userId", value = userId } };
            contents = notification?.contents ?? new { en = "" }; ;
            buttons = notification?.buttons;
            headings = notification?.headings;
            large_icon = notification?.large_icon;
            if (notification?.IsPopup ?? false)
                android_channel_id = options.PopupChannelId;
        }


    }
}
