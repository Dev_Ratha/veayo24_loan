﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;

namespace Veayo24.PS.Lib.OneSignal
{
    public class Notification : INotification
    {
        public virtual string Code { get; set; } = string.Empty;
        public virtual string GenCode { get; set; } = string.Empty;
        public virtual int Version { get; set; } = 0;
        public IEnumerable<NotificationSubject> Subjects { get; set; } = Enumerable.Empty<NotificationSubject>();
        public IEnumerable<NotificationObject> Objects { get; set; } = Enumerable.Empty<NotificationObject>();
        public virtual bool IsPopup { get; set; } = true;
        public IEnumerable<string> Audiences { get; set; } = Enumerable.Empty<string>();
        public OneSignalOptions Options { get; set; }
        public object Data { get; set; } = new object();
        public string NotificationType { get; set; } = string.Empty;
        public virtual bool IsSaveDb { get; set; } = true;
        public string? CircleId { get; set; }

        public Notification()
        {

        }
        public Notification(OneSignalOptions options)
        {
            Options = options;
        }

        public Notification(IEnumerable<NotificationSubject> subjects, NotificationObject @object, OneSignalOptions options) : this(subjects, new NotificationObject[] { @object }, options) { }
        public Notification(IEnumerable<NotificationSubject> subjects, IEnumerable<NotificationObject> objects, OneSignalOptions options)
        {
            Subjects = subjects;
            Objects = objects;
            Options = options;
        }

        public string GenerateContent<TResource>(IStringLocalizer<TResource> stringLocalizer, string audienceId)
        {

            var resource = stringLocalizer[GetResourceKey()];

            if (resource.ResourceNotFound == true)
                return string.Empty;

            var html = string.Format(resource.Value, GetMsgParams(audienceId).ToArray());

            return Regex.Replace(html, "<.*?>", string.Empty);
        }

        public virtual IEnumerable<string> GetMsgParams(string audienceId)
        {
            return new string[] { };
        }

        public string GetResourceKey()
        {
            return $"{Code}_{Version}";
        }

        public virtual Task<IEnumerable<INotificationPush>> GetNotificationPushesAsync()
            => Task.Run(() => { return Enumerable.Empty<INotificationPush>(); });

        public void AddSubject(NotificationSubject subject)
        {
            Subjects = new HashSet<NotificationSubject>(Subjects) { subject };
        }

        public void AddAudiences(IEnumerable<string> audiences)
        {
            var auds = new HashSet<string>(Audiences);
            foreach (var aud in audiences)
                auds.Add(aud);

            Audiences = auds;
        }
        public void AddObject(NotificationObject obj)
        {
            Objects = new HashSet<NotificationObject>(Objects) { obj };
        }

        public virtual void Init(IServiceProvider serviceProvider)
        {
            Options = serviceProvider.GetService<OneSignalOptions>();
        }

        public bool CheckExistingSubject(NotificationSubject subject)
        {
            return Subjects.Where(s => s.Id == subject.Id).Any();
        }

        public void RemoveAudience(string audienceId)
        {
            var auds = new HashSet<string>(Audiences);
            auds.Remove(audienceId);
            Audiences = auds;
        }

        public INotification SimplifySubsAndObjs(string userId)
        {
            var clone = (INotification)MemberwiseClone();
            // remove subject that is audience
            var subs = new HashSet<NotificationSubject>(Subjects);
            subs.RemoveWhere(s => s.Id == userId);
            clone.Subjects = subs;

            var objs = new HashSet<NotificationObject>(Objects);
            objs.RemoveWhere(s => s.UserId == userId);
            clone.Objects = objs;

            return clone;
        }


    }
}
