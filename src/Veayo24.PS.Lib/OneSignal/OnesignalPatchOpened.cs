﻿using System.Net.Http;
using System.Text;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Veayo24.PS.Lib.OneSignal
{
    public class OnesignalPatchOpened
    {

        public bool Opened { get; set; } = true;
        [JsonProperty("app_id")]
        public string App_Id { get; set; }

        public OnesignalPatchOpened(string app_Id)
        {
            App_Id = app_Id;
        }

        public StringContent GetJsonPayload()
         => new StringContent(JsonConvert.SerializeObject(this, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() }),
                    Encoding.UTF8, "application/json");
    }
}
