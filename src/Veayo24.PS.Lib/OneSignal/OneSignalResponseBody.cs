﻿using Newtonsoft.Json;

namespace Veayo24.PS.Lib.OneSignal
{
    public class OneSignalResponseBody
    {
        [JsonProperty("id")]
        public string Id { get; set; }
        [JsonProperty("recipients")]
        public int Recipients { get; set; }
        [JsonProperty("external_id")]
        public string ExternalId { get; set; }

    }
}
