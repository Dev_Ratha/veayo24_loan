﻿using System;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Localization;
using Newtonsoft.Json.Linq;

namespace Veayo24.PS.Lib.OneSignal
{
    public static class Extentions
    {
        private const string SectionName = "OneSignal";
        public static IServiceCollection AddOneSignal(this IServiceCollection services)
        {
            var oneSignalOptions = services.GetOptions<OneSignalOptions>(SectionName);
            services.AddSingleton(oneSignalOptions);

            services.AddHttpClient("OneSignal", c =>
            {
                c.BaseAddress = new Uri(oneSignalOptions.URL);
                c.DefaultRequestHeaders.Add("ContentType", "application/vnd.github.v3+json");
                c.DefaultRequestHeaders.Add("Authorization", $"Basic {oneSignalOptions.ApiKey}");
            });

            services.AddTransient<IOneSignal, OneSignal>();
            services.AddTransient<INotificationDb, DefaultNotificationDb>();
            return services;
        }

        public static string GenerateContent<TResource>(this ONotificationBase notification, IStringLocalizer<TResource> stringLocalizer)
        {
            var html = notification.GenerateHtmlContent(stringLocalizer);

            return Regex.Replace(html, "<.*?>", string.Empty);
        }

        public static string GenerateHtmlContent<TResource>(this ONotificationBase notification, IStringLocalizer<TResource> stringLocalizer)
        {
            var resource = stringLocalizer[notification.GetResourceKey()];

            if (resource.ResourceNotFound == true)
                return string.Empty;

            return string.Format(resource.Value, notification.GetMsgParams().ToArray());
        }

        public static string GetResourceKey(this ONotificationBase noti)
           => $"{noti.Code}_{noti.Version}";

        public static async Task<string> PushNowAsync(this INotificationPush push, IServiceProvider sp)
        {
            push.include_player_ids = push.include_player_ids.Where(x => x != string.Empty).ToArray();

            if (push.include_player_ids.Count() <= 0)
                return "";

            var _clientFactory = sp.GetService<IHttpClientFactory>();
            var client = _clientFactory.CreateClient("OneSignal");
            var response = await client.PostAsync("", push.GetJsonPayload());
            var content = await response.Content.ReadAsStringAsync();
            if (response.StatusCode == HttpStatusCode.OK)
            {
                var body = Newtonsoft.Json.JsonConvert.DeserializeObject<OneSignalResponseBody>(content);
                return body?.Id ?? string.Empty;
            }
            return "";
        }

        public static async Task RemoveOnsignalNowAsync(this IServiceProvider sp, string notificationId)
        {
            var configuration = sp.GetService<IConfiguration>();
            var option = configuration.GetOptions<OneSignalOptions>(SectionName);
            var _clientFactory = sp.GetService<IHttpClientFactory>();
            var client = _clientFactory.CreateClient("OneSignal");
            var response = await client.PutAsync($"{notificationId}", new OnesignalPatchOpened(option.AppId).GetJsonPayload());
            var cont = await response.Content.ReadAsStringAsync();

        }

        public static T? ValueToObject<T>(this NotificationObject obj) where T : class
        {
            if (obj.Value is null)
                return null;

            if (obj.Value.GetType() == typeof(JObject))
            {
                var re = obj.Value as JObject;
                T? tt;
                try
                {
                    tt = re?.ToObject<T>();
                }
                catch (Exception e)
                {
                    throw e;
                }

                return tt;
            }

            return obj.Value as T;
        }
    }
}
