﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.OneSignal
{
    public class DefaultNotificationDb : INotificationDb
    {
        public Task SaveAsync(string userId, ONotificationBase noti)
        {
            return Task.CompletedTask;
        }

        public Task SaveAsync(string userId, INotification noti, string notificationId)
        {
            return Task.CompletedTask;
        }
    }
}
