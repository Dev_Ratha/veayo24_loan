﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Newtonsoft.Json;

namespace Veayo24.PS.Lib.OneSignal
{
    public class NotificationSubject : IEquatable<NotificationSubject>
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string ProfilePicture { get; set; }
        public SubjectGallery ProfilePhoto { get; set; }

        [JsonConstructor]
        public NotificationSubject(string id, string name, string profileUrl, SubjectGallery gallery)
        {
            Id = id;
            Name = name;
            ProfilePicture = profileUrl;
            ProfilePhoto = gallery;
        }

        public NotificationSubject(int id, string name, string profileUrl, SubjectGallery gallery)
        {
            Id = id.ToString();
            Name = name;
            ProfilePicture = profileUrl;
            ProfilePhoto = gallery;
        }
        public NotificationSubject(int id, string name, string profileUrl)
        {
            Id = id.ToString();
            Name = name;
            ProfilePicture = profileUrl;
        }

        public static IEnumerable<NotificationSubject> Empty()
            => Enumerable.Empty<NotificationSubject>();

        public bool Equals([AllowNull] NotificationSubject other)
        {
            if (ReferenceEquals(null, other)) return false;
            return ReferenceEquals(this, other) || Id.Equals(other.Id);
        }

        public override int GetHashCode()
        {
            return Id.GetHashCode();
        }
    }
}
