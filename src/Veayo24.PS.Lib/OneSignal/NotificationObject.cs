﻿namespace Veayo24.PS.Lib.OneSignal
{
    public class NotificationObject : INotificationObject
    {
        public string UserId { get; set; }
        public object Value { get; set; }

        public NotificationObject(string userId, object value)
        {
            UserId = userId;
            Value = value;
        }
    }
}
