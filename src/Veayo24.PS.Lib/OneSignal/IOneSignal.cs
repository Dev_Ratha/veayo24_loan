﻿using System;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.OneSignal
{
    public interface IOneSignal
    {
        Task PushAsync<T>(T data, string userId, bool saveDB = false, TimeSpan delay = default) where T : ONotificationBase;
    }
}
