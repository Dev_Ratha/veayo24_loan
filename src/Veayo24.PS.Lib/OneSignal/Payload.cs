﻿namespace Veayo24.PS.Lib.OneSignal
{
    public class Payload
    {
        public string Code { get; private set; } = string.Empty;
        public object Data { get; private set; } = new object();

        public Payload(object data, string code)
        {
            Code = code;
            Data = data;
        }
    }
}
