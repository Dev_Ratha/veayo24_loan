﻿using System;
using System.Collections.Generic;
using Microsoft.Extensions.Localization;

namespace Veayo24.PS.Lib.OneSignal
{
    public interface INotification
    {
        //public string AudienceId { get; set; }
        public string Code { get; set; }
        public string GenCode { get; set; }
        public int Version { get; set; }
        public string? CircleId { get; set; }
        public bool IsPopup { get; set; }
        public IEnumerable<NotificationSubject> Subjects { get; set; }
        public IEnumerable<NotificationObject> Objects { get; set; }
        public IEnumerable<string> Audiences { get; set; }
        public OneSignalOptions Options { get; set; }
        public object Data { get; set; }
        public string NotificationType { get; set; }
        public bool IsSaveDb { get; set; }

        void Init(IServiceProvider serviceProvider);
        void AddSubject(NotificationSubject subject);
        void AddObject(NotificationObject obj);
        IEnumerable<string> GetMsgParams(string audienceId);
        string GenerateContent<TResource>(IStringLocalizer<TResource> stringLocalizer, string audienceId);
        string GetResourceKey();
        void RemoveAudience(string audienceId);
        INotification SimplifySubsAndObjs(string audienceId);

    }
}
