﻿namespace Veayo24.PS.Lib.OneSignal
{
    public class SubjectGallery
    {
        public string? MediaId { get; set; }
        public string? Url { get; set; }
        public string? ThumbUrl { get; set; }
        public string? PublicId { get; set; }

        public SubjectGallery(string? mediaId, string? url, string? thumbUrl, string? publicId)
        {
            MediaId = mediaId;
            Url = url;
            ThumbUrl = thumbUrl;
            PublicId = publicId;
        }

        public static SubjectGallery Empty()
            => new SubjectGallery(string.Empty, string.Empty, string.Empty, string.Empty);
    }
}
