﻿namespace Veayo24.PS.Lib.OneSignal
{
    public interface INotificationObject
    {
        string UserId { get; set; }
    }
}
