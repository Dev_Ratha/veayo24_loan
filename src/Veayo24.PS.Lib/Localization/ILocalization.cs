﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Localization
{
    public interface ILocalization
    {
        Task<string> GetStringAsync(string key, string region = "");
    }
}
