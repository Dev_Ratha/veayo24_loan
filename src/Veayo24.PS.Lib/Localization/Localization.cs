﻿using System;
using System.Globalization;
using System.Resources;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Lib.Localization.Resources;

namespace Veayo24.PS.Lib.Localization
{
    public class Localization : ILocalization
    {
        private readonly IServiceProvider _serviceProvider;

        public Localization(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }
        public Task<string> GetStringAsync(string key, string region = "")
        => Task.Run(() =>
        {
            var optinos = _serviceProvider.GetService<LocalizationOptions>();
            var cultureInfo = new CultureInfo(region);
            var resourceManager = new ResourceManager(optinos.Path, typeof(Language).Assembly);
            return resourceManager.GetString(key, cultureInfo) ?? string.Empty;
        });

    }
}
