﻿using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Veayo24.PS.Lib.Utils
{
    public static class Extensions
    {
        public static T Field<T, TValue>(this T target, Expression<Func<T, TValue>> memberLamda, TValue value) where T : class, new()
        {
            var memberSelectorExpression = memberLamda.Body as MemberExpression;
            if (memberSelectorExpression != null)
            {
                var property = memberSelectorExpression.Member as PropertyInfo;
                if (property != null)
                {
                    property.SetValue(target, value, null);
                }
            }
            return target;
        }

    }
}
