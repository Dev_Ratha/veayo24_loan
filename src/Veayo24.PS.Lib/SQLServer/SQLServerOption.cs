﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veayo24.PS.Lib.EntityFramework
{
    public class SQLServerOption
    {
        public string ConnectionStrings { get; set; } = string.Empty;
        public string DefaultUser { get; set; } = string.Empty;
        public string DefaultUserPassword { get; set; } = string.Empty;
    }
}
