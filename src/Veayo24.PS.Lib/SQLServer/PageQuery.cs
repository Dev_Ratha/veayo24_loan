﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Veayo24.PS.Lib.EntityFramework
{
    public class PageQuery
    {
        public int PageId { get; set; } = 0;
        public int Result { get; set; } = 10;
        public string Sort { get; set; } = string.Empty;
        public string Search { get; set; } = string.Empty;
    }
}
