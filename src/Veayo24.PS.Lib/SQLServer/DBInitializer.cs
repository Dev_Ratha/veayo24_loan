﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.EntityFramework
{
    public class DBInitializer : IDBInitializer
    {
        private static int _initialized;
        private readonly IDBSeeder _seeder;

        public DBInitializer(IDBSeeder seeder)
        {
            _seeder = seeder;
        }
        public Task InitializeAsync()
        {
            if (Interlocked.Exchange(ref _initialized, 1) == 1)
            {
                return Task.CompletedTask;
            }

            return _seeder.SeedAsync();
        }
    }
}
