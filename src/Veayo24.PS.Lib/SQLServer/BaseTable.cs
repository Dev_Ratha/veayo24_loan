﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
namespace Veayo24.PS.Lib.EntityFramework
{
    public class BaseTable
    {
        [Key]
        public long Id { get; set; }
    }
}
