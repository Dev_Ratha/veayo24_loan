﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.EntityFramework
{
    public interface IDBSeeder
    {
        Task SeedAsync();
    }
}
