﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace Veayo24.PS.Lib.EntityFramework
{
    public class BasePagination
    {
        public int TotalPageCount { get; set; }
        public int ItemCount { get; set; }
        public int AllItemCount { get; set; }
        public int CurrentPage { get; set; }
        public int Result { get; set; }
        public bool HasNextPage => this.CurrentPage < this.TotalPageCount;
        public bool HasPreviousPage => this.CurrentPage > 1;


    }

    public class Pagination<T> : BasePagination
    {
        public IEnumerable<T> DataList { get; set; }

        public Pagination() { }
        public Pagination(IEnumerable<T> dataList, int currentPage, int itemCount, int totalPageCount, int allItemCount, int result)
        {
            DataList = dataList;
            CurrentPage = currentPage;
            ItemCount = itemCount;
            TotalPageCount = totalPageCount;
            AllItemCount = allItemCount;
            Result = result;
        }

        public Pagination<T> From<T>(BasePagination result, IEnumerable<T> items)
            => new Pagination<T>(items, result?.CurrentPage ?? 1, result?.ItemCount ?? 0, result?.TotalPageCount ?? 0, result?.AllItemCount ?? 0, result?.Result ?? 0);

        public Pagination<U> Map<U>(Func<T, U> map)
            => From(this, DataList.Select(map));

    }

    public static class PaginationExtension
    {
        public static Pagination<T> ToPagination<T>(this IQueryable<T> queryable, int page, int size)
        {
            var p = new Pagination<T>();
            p.AllItemCount = queryable.Count();
            p.CurrentPage = page;
            p.TotalPageCount = (int)Math.Ceiling(p.AllItemCount / (double)size);
            var list = queryable.Skip((page - 1) * size).Take(size).ToList();
            p.ItemCount = list.Count;
            p.DataList = list;
            p.Result = size;
            return p;
        }
        public static Pagination<T> ToPagination<T>(this List<T> queryable, int page, int size)
        {
            var p = new Pagination<T>();
            p.AllItemCount = queryable.Count();
            p.CurrentPage = page;
            p.TotalPageCount = (int)Math.Ceiling(p.AllItemCount / (double)size);
            var list = queryable.Skip((page - 1) * size).Take(size).ToList();
            p.ItemCount = list.Count;
            p.DataList = list;
            p.Result = size;
            return p;
        }
        public static async Task<Pagination<T>> ToPaginationAsync<T>(this IQueryable<T> queryable, int? page, int? size)
        {
            var p = new Pagination<T>();
            p.AllItemCount = await queryable.CountAsync();
            p.CurrentPage = page ?? 1;
            p.TotalPageCount = (int)Math.Ceiling(p.AllItemCount / (double)size);
            int c = ((page ?? 1) - 1) * (size ?? 10);
            var q = queryable.Skip(c).Take((size ?? 10));
            var list = await q.ToListAsync();
            p.ItemCount = list.Count();
            p.DataList = list;
            p.Result = (size ?? 10);
            return p;
        }
        public static async Task<Pagination<T>> ToPagination<T>(this Task<List<T>> queryable, int page, int size)
        {
            var lst = await queryable;
            var p = new Pagination<T>();
            p.AllItemCount = lst.Count();
            p.CurrentPage = page;
            p.TotalPageCount = (int)Math.Ceiling(p.AllItemCount / (double)size);
            var list = lst.Skip((page - 1) * size).Take(size).ToList();
            p.ItemCount = list.Count;
            p.DataList = list;
            p.Result = size;
            return p;
        }
        public static async Task<List<T>> ToListPaging<T>(this Task<List<T>> queryable, int page, int size)
        {
            var lst = await queryable;
            var list = lst.Skip((page - 1) * size).Take(size).ToList();
            return list;
        }
        public static async Task<List<T>> ToListPagingAsync<T>(this IQueryable<T> queryable, int page, int size)
        {
            int c = (page - 1) * size;
            var q = queryable.Skip(c).Take(size);
            var list = await q.ToListAsync();
            return list;
        }


    }


}
