﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Veayo24.PS.Lib.EntityFramework
{
    public interface IContext
    {
        public DbContext Context { get; set; }
    }
}
