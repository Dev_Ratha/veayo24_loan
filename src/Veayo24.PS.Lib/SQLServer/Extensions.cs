﻿using Microsoft.Data.SqlClient;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Veayo24.PS.Lib.EntityFramework
{
    public static class Extensions
    {

        public static IServiceCollection AddSQLServer<TContext>(this IServiceCollection services) where TContext : DbContext
        {

            var sqlOption = services.GetOptions<SQLServerOption>("SQLServer");
            services.AddSingleton(sqlOption);

            services.AddDbContext<TContext>(option => option.UseSqlServer(sqlOption.ConnectionStrings).UseQueryTrackingBehavior(QueryTrackingBehavior.NoTracking));
            services.AddTransient<IDBInitializer, DBInitializer>();
            services.AddTransient<IDBSeeder, DefaultDBSeeder>();
            return services;
        }



        public static IndexBuilder NpgsqlIncludeProperty<T>(this IndexBuilder<T> ibuilder, System.Linq.Expressions.Expression<Func<T, object>> predicate)
        {
            return NpgsqlIndexBuilderExtensions.IncludeProperties(ibuilder, predicate);
        }

        private static List<T> AutoMap<T>(this IDataReader dr)
        {
            List<T> list = new List<T>();
            T obj = default(T);
            while (dr.Read())
            {
                obj = Activator.CreateInstance<T>();
                if (obj != null)
                {
                    foreach (PropertyInfo prop in obj.GetType().GetProperties())
                    {
                        if (!object.Equals(dr[prop.Name], DBNull.Value))
                        {
                            prop.SetValue(obj, dr[prop.Name], null);
                        }
                    }
                    list.Add(obj);
                }

            }
            return list;
        }

        public static async Task<List<T>> ExecuteProc<T>(this DbContext context, string sqlCmd, params SqlParameter[] parameters)
        {
            DbConnection con = context.Database.GetDbConnection();
            if (con.State == ConnectionState.Closed)
                await con.OpenAsync();
            var cmd = con.CreateCommand();
            cmd.CommandText = sqlCmd;
            if (parameters != null && parameters.Length > 0)
                foreach (var p in parameters)
                    cmd.Parameters.Add(p);
            var reader = await cmd.ExecuteReaderAsync();
            var lst = reader.AutoMap<T>();
            con.Close();
            return lst;
        }
        public static async Task<List<T>> ExecuteProc<T>(this DbContext context, string sqlCmd)
        {
            DbConnection con = context.Database.GetDbConnection();
            if (con.State == ConnectionState.Closed)
                await con.OpenAsync();
            var cmd = con.CreateCommand();
            cmd.CommandText = sqlCmd;
            var reader = await cmd.ExecuteReaderAsync();
            var lst = reader.AutoMap<T>();
            con.Close();
            return lst;
        }
    }
}
