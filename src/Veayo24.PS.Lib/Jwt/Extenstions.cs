﻿using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.Jwt
{
    public static class Extenstions
    {
        private const string SectionName = "jwt";
        public static IServiceCollection AddJwt(this IServiceCollection services)
        {
            var option = services.GetOptions<JwtOptions>(SectionName);
            services.AddSingleton(option);

            return services;
        }
    }
}
