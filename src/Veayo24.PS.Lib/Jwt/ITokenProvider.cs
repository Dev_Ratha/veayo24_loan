﻿using System.Security.Claims;

namespace Veayo24.PS.Lib.Jwt
{
    public interface ITokenProvider<TUser> where TUser : class
    {
        string CreateToken(TUser user, string userId);
        string CreateToken(string id, string code);
        string CreateToken(Claim[] claims);
        bool ValidateToken(string token);
    }
}
