﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.Types
{
    public interface IInitializer
    {
        Task InitializeAsync();
    }
}
