﻿namespace Veayo24.PS.Lib.Types
{
    public interface IIdentifiable<out T>
    {
        T Id { get; }
    }
}
