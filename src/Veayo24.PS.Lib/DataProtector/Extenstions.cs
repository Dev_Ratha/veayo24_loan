﻿using System;
using System.IO;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.DataProtector
{
    public static class Extenstions
    {
        private const string SectionName = "DataProtector";
        public static IServiceCollection AddDataProtector(this IServiceCollection services)
        {
            var options = services.GetOptions<DataProtectorOptions>(SectionName);
            var path = options.PersistKeyPath;
            if (path.Equals(string.Empty))
                path = AppDomain.CurrentDomain.BaseDirectory!;

            services.AddSingleton(options);
            services.AddDataProtection()
                .PersistKeysToFileSystem(new DirectoryInfo(path));

            var provider = services.BuildServiceProvider().GetService<IDataProtectionProvider>();
            var protector = provider.CreateProtector(options.ProtectorKey);
            services.AddSingleton(protector);

            return services;
        }
    }
}
