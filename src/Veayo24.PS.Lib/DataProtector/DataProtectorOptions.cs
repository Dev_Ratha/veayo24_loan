﻿namespace Veayo24.PS.Lib.DataProtector
{
    public class DataProtectorOptions
    {
        public string ProtectorKey { get; set; }
        public string PersistKeyPath { get; set; }
    }
}
