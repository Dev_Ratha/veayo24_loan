﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.Postgre
{
    public static class Extensions
    {
        public static IServiceCollection AddPostgre<TContext>(this IServiceCollection services) where TContext : DbContext
        {
            var postgreOption = services.GetOptions<PostgreOption>("ConnectionStrings");

            services.AddEntityFrameworkNpgsql();
            services.AddDbContext<TContext>(option => option.UseNpgsql(postgreOption.Default));

            return services;
        }
    }
}
