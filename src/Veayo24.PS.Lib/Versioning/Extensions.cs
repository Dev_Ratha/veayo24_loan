﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.DependencyInjection;

namespace Veayo24.PS.Lib.Versioning
{
    public static class Extensions
    {
        public static IServiceCollection AddVersioning(this IServiceCollection services)
        {
            services.AddVersionedApiExplorer(
                  options =>
                  {
                      options.GroupNameFormat = "'v'V";
                      options.SubstituteApiVersionInUrl = true;
                  }
                );

            services.AddApiVersioning(config =>
            {
                config.DefaultApiVersion = ApiVersion.Parse("1");
                config.AssumeDefaultVersionWhenUnspecified = true;
                config.ReportApiVersions = true;
                config.ApiVersionReader = new HeaderApiVersionReader("api-version");
            });
            return services;
        }
    }
}
