﻿using System;

namespace Veayo24.PS.Lib.Exceptions
{
    public class InvalidPhoneException : BaseException
    {
        public override string Code => "invalid_phone";

        public InvalidPhoneException(string phone) : base($"Invalid phone number {phone}")
        {

        }

        public InvalidPhoneException()
        {
        }

        public InvalidPhoneException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
