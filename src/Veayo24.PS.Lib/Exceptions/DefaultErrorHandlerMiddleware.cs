﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Veayo24.PS.Lib.Exceptions
{
    public sealed class DefaultErrorHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<DefaultErrorHandlerMiddleware> _logger;

        public DefaultErrorHandlerMiddleware(RequestDelegate next, ILogger<DefaultErrorHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (Exception exception)
            {
                var additionalData = new object();
                if (exception is BaseException)
                    additionalData = (exception as BaseException)!.AdditionalData;

                _logger.LogError(exception, $"{exception.Message}{"{@AdditionalData}"}", additionalData);
                await HandleErrorAsync(context, exception);
            }
        }

        private static Task HandleErrorAsync(HttpContext context, Exception exception)
        {
            var statusCode = 400;



            var response = new { code = statusCode, message = exception.Message };

            var payload = JsonConvert.SerializeObject(response, new JsonSerializerSettings { ContractResolver = new CamelCasePropertyNamesContractResolver() });
            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;

            return context.Response.WriteAsync(payload);
        }
    }
}
