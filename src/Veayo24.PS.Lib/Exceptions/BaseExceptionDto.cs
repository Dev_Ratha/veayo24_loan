﻿namespace Veayo24.PS.Lib.Exceptions
{
    public class BaseExceptionDto
    {
        public string Code { get; set; }
        public int StatusCode { get; set; }
    }
}
