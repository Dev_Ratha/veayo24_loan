﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Mvc.ApiExplorer;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.OpenApi.Models;
using Veayo24.PS.Lib.Versioning;
using Swashbuckle.AspNetCore.Filters;
using Swashbuckle.AspNetCore.SwaggerGen;

namespace Veayo24.PS.Lib.Swagger
{
    public static class Extensions
    {
        public static IServiceCollection AddSwagger(this IServiceCollection services)
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {

                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Veayo24 PS Development APIs", Version = "v1" });

                c.ExampleFilters();

                //c.OperationFilter<AddHeaderOperationFilter>("correlationId", "Correlation Id for the request", false); // adds any string you like to the request headers - in this case, a correlation id

                c.OperationFilter<AddResponseHeadersFilter>(); // [SwaggerResponseHeader]

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                //c.OperationFilter()
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {new OpenApiSecurityScheme {Reference = new OpenApiReference{ Id= "Bearer", Type = ReferenceType.SecurityScheme} }, new List<string>()}
                });

                var filePath = Path.Combine(AppContext.BaseDirectory, "Veayo24.PS.Api.xml");
                c.IncludeXmlComments(filePath);
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>(); // Adds "(Auth)" to the summary so that you can see which endpoints have Authorization
                                                                              // or use the generic method, e.g. c.OperationFilter<AppendAuthorizeToSummaryOperationFilter<MyCustomAttribute>>();

                // add Security information to each operation for OAuth2
                //c.OperationFilter<SecurityRequirementsOperationFilter>();
            });
            services.AddSwaggerExamplesFromAssemblyOf<TestExample>();
            return services;
        }
        public static IServiceCollection AddSwagger<THeader>(this IServiceCollection services) where THeader : IOperationFilter
        {
            // Register the Swagger generator, defining 1 or more Swagger documents
            services.AddSwaggerGen(c =>
            {
                var provider = services.BuildServiceProvider().GetRequiredService<IApiVersionDescriptionProvider>();

                foreach (var description in provider.ApiVersionDescriptions)
                {
                    c.SwaggerDoc(description.GroupName, new OpenApiInfo
                    {
                        Title = $"Veayo24 PS Development APIs",
                        Version = $"v{description.ApiVersion}"
                    });
                }
                c.ExampleFilters();

                c.OperationFilter<AddResponseHeadersFilter>(); // [SwaggerResponseHeader]

                var security = new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }},
                };
                c.OperationFilter<THeader>();

                //remove parameter {version}
                c.OperationFilter<RemoveVersionParameterFilter>();
                //replace {version} with actual value
                c.DocumentFilter<ReplaceVersionWithExactValueInPathFilter>();

                c.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme()
                {
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey
                });
                //c.OperationFilter()
                c.AddSecurityRequirement(new OpenApiSecurityRequirement
                {
                    {new OpenApiSecurityScheme {Reference = new OpenApiReference{ Id= "Bearer", Type = ReferenceType.SecurityScheme} }, new List<string>()}
                });

                var filePath = Path.Combine(AppContext.BaseDirectory, "Veayo24.PS.Api.xml");
                c.IncludeXmlComments(filePath);
                c.OperationFilter<AppendAuthorizeToSummaryOperationFilter>(); // Adds "(Auth)" to the summary so that you can see which endpoints have Authorization
                                                                              // or use the generic method, e.g. c.OperationFilter<AppendAuthorizeToSummaryOperationFilter<MyCustomAttribute>>();
                                                                              // add Security information to each operation for OAuth2
                                                                              //c.OperationFilter<SecurityRequirementsOperationFilter>();
            });
            services.AddSwaggerExamplesFromAssemblyOf<TestExample>();
            return services;
        }
    }
}
