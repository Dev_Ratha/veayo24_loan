﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.CQRS.Commands
{
    public interface ICommandHandler<in TCommand, TId> where TCommand : class, ICommand
    {
        Task HandleAsync(TCommand command, TId id);
    }

    public interface ICommandHandler<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command);
    }

}
