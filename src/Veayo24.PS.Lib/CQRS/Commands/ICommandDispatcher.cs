﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.CQRS.Commands
{
    public interface ICommandDispatcher
    {
        Task PerformAsync<T, TId>(T command, TId id) where T : class, ICommand;
        Task PerformAsync<T>(T command) where T : class, ICommand;
    }
}

