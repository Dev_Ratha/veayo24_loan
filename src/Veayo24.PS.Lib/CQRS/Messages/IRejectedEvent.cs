﻿using Veayo24.PS.Lib.CQRS.Events;

namespace Veayo24.PS.Lib.Messages
{
    public interface IRejectedEvent : IEvent
    {
        string Reason { get; }
        string Code { get; }
    }
}
