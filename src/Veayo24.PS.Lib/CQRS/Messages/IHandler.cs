﻿using System;
using System.Threading.Tasks;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Lib.HandlersCorrelationContext
{
    public interface IHandler
    {
        IHandler Handle(Func<Task> handle);
        IHandler OnSuccess(Func<Task> onSuccess);
        IHandler OnError(Func<Exception, Task> onError, bool rethrow = false);
        IHandler OnCustomError(Func<BaseException, Task> onCustomError, bool rethrow = false);
        IHandler Always(Func<Task> always);
        Task ExecuteAsync();
    }
}
