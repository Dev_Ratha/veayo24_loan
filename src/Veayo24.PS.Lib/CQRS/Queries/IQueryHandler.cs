﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.CQRS.Queries
{
    public interface IQueryHandler<in TQuery, TResult> where TQuery : class, IQuery<TResult>
    {
        Task<TResult>? HandleAsync(TQuery query);
    }
}
