﻿namespace Veayo24.PS.Lib.CQRS.Queries
{
    public class PagedQueryBase : IPagedQuery
    {
        public int Page { get; set; }
        public int Results { get; set; }
    }
}
