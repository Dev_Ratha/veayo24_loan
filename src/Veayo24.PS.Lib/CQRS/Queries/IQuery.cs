﻿namespace Veayo24.PS.Lib.CQRS.Queries
{
    public interface IQuery
    {
    }

    public interface IQuery<T> : IQuery
    {
    }
}
