﻿namespace Veayo24.PS.Lib.CQRS.Queries
{
    public interface IPagedQuery : IQuery
    {
        int Page { get; }
        int Results { get; }
    }
}
