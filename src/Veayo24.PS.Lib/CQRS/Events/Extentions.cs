﻿using System;
using Microsoft.Extensions.DependencyInjection;


namespace Veayo24.PS.Lib.CQRS.Events
{
    public static class Extentions
    {
        public static IServiceCollection AddEventHandlers(this IServiceCollection services)
        {
            services.Scan(s =>
               s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                   .AddClasses(c => c.AssignableTo(typeof(IEventHandler<>)))
                   .AsImplementedInterfaces()
                   .WithTransientLifetime());

            return services;
        }
    }
}
