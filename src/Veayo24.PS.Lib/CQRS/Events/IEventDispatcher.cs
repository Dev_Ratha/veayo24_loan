﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.CQRS.Events
{
    public interface IEventDispatcher
    {
        Task PublishAsync<T>(T @event) where T : class, IEvent;
    }
}
