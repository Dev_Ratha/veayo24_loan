﻿using System.Threading.Tasks;

namespace Veayo24.PS.Lib.CQRS.Events
{
    public interface IEventHandler<in TEvent> where TEvent : IEvent
    {
        Task HandleAsync(TEvent @event);
    }
}
