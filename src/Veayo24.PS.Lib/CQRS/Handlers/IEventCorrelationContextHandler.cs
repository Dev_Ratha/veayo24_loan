﻿using System.Threading.Tasks;
using Veayo24.PS.Lib.CQRS.Events;

namespace Veayo24.PS.Lib.HandlersCorrelationContext
{
    public interface IEventCorrelationContextHandler<in TEvent> where TEvent : IEvent
    {
        Task HandleAsync(TEvent @event);
    }
}
