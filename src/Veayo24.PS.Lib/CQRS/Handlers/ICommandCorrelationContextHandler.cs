﻿using System.Threading.Tasks;
using Veayo24.PS.Lib.CQRS.Commands;

namespace Veayo24.PS.Lib.HandlersCorrelationContext
{
    public interface ICommandCorrelationContextHandler<in TCommand> where TCommand : ICommand
    {
        Task HandleAsync(TCommand command);
    }
}
