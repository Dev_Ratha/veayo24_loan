﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Veayo24.PS.Lib.HandlersCorrelationContext;

namespace Veayo24.PS.Lib.CQRS.Handlers
{
    public static class Extensions
    {
        public static IServiceCollection AddCommandCorrelationContextHandlers(this IServiceCollection services)
        {
            services.Scan(s =>
                s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                    .AddClasses(c => c.AssignableTo(typeof(ICommandCorrelationContextHandler<>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            return services;
        }
        public static IServiceCollection AddEventCorrelationContextHandlers(this IServiceCollection services)
        {
            services.Scan(s =>
                s.FromAssemblies(AppDomain.CurrentDomain.GetAssemblies())
                    .AddClasses(c => c.AssignableTo(typeof(IEventCorrelationContextHandler<>)))
                    .AsImplementedInterfaces()
                    .WithTransientLifetime());

            return services;
        }
    }
}
