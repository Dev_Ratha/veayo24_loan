﻿using System.Collections.Generic;

namespace Veayo24.PS.Lib.Logging
{
    public class LoggerOptions
    {
        public string? ApplicationName { get; set; }
        public string? ServiceId { get; set; }
        public ConsoleOptions? Console { get; set; }
        public SeqOptions? Seq { get; set; }
        public IEnumerable<string>? ExcludePaths { get; set; }
    }
}
