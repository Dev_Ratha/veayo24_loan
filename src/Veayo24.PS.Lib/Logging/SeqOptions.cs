﻿namespace Veayo24.PS.Lib.Logging
{
    public class SeqOptions
    {
        public bool Enabled { get; set; }
        public string Url { get; set; } = string.Empty;
        public string ApiKey { get; set; } = string.Empty;
    }
}
