﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Dto.Users;
using Veayo24.PS.Lib.CQRS.Queries;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Application.Queries.Users
{
    public class GetUserPageByBranchQuery : IQuery<Pagination<UserDto>>
    {
        public long? BranchId { get; set; }
        public int PageId { get; set; }
        public int Result { get; set; }

        public GetUserPageByBranchQuery(long? branchId, int pageId, int result)
        {
            BranchId = branchId;
            PageId = pageId;
            Result = result;
        }
    }
}
