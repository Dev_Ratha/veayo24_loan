﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Dto.UserRoles;
using Veayo24.PS.Lib.CQRS.Queries;

namespace Veayo24.PS.Application.Queries.UserRoles
{
    public class GetUserRoleListQuery : IQuery<IEnumerable<UserRoleDto>>
    {
        public long BranchId { get; set; }

        public GetUserRoleListQuery(long branchId)
        {
            BranchId = branchId;
        }
    }
}
