﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.CQRS.Commands;

namespace Veayo24.PS.Application.Commands.Users
{
    public class CreateUserCommand : ICommand
    {
        public long BranchId { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public long? UserRoleId { get; set; }

        public CreateUserCommand(long branchId, string fullName, string userName, string password, long? userRoleId)
        {
            BranchId = branchId;
            FullName = fullName;
            UserName = userName;
            Password = password;
            UserRoleId = userRoleId;
        }
    }
}
