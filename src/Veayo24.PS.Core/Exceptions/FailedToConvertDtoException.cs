﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class FailedToConvertDtoException : BaseException
    {
        public override string Code => "failed_convert_dto";
        public new int StatusCode { get; } = 500;

        public FailedToConvertDtoException()
        {
        }

        public FailedToConvertDtoException(string message) : base(message)
        {
        }

        public FailedToConvertDtoException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
