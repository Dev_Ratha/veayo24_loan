﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class NullQueryException : BaseException
    {
        public override string Code => "null_query";
        public new int StatusCode { get; } = 500;

        public NullQueryException()
        {
        }

        public NullQueryException(string message) : base(message)
        {
        }

        public NullQueryException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
