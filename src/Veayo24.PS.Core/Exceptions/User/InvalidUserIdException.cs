﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class InvalidUserIdException : BaseException
    {
        public override string Code => "invalid_userid";

        public InvalidUserIdException(string userId) : base($"Invalid user id {userId}") { }

        public InvalidUserIdException()
        {
        }

        public InvalidUserIdException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
