﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.Circle
{
    public class MembershipExpiredException : BaseException
    {
        public override string Code => "membership_expired_exceeded";

        public MembershipExpiredException(string userId) : base($"User {userId} membership has been expired", 402)
        {

        }

        public MembershipExpiredException()
        {
        }

        public MembershipExpiredException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
