﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class InvalidPasswordException : BaseException
    {
        public override string Code => "invalid_password";

        public InvalidPasswordException(string message) : base(message)
        {
        }

        public InvalidPasswordException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public InvalidPasswordException()
        {
        }
    }
}
