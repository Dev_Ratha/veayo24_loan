﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class ExistedPhoneException : BaseException
    {
        public override string Code => "existed_phone";

        public ExistedPhoneException(string phone) : base($"Duplicate phone {phone}")
        {

        }

        public ExistedPhoneException()
        {
        }

        public ExistedPhoneException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
