﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class InvalidGenderException : BaseException
    {
        public override string Code => "invalid_gender";

        public InvalidGenderException(string gender) : base($"Invalid gender {gender}") { }

        public InvalidGenderException()
        {
        }

        public InvalidGenderException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
