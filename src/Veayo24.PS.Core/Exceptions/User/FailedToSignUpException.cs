﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class FailedToSignUpException : BaseException
    {
        public override string Code => "signup_failed";

        public FailedToSignUpException(string message) : base(message)
        {
        }

        public FailedToSignUpException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public FailedToSignUpException()
        {
        }
    }
}
