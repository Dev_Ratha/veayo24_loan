﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class AccountLockedException : BaseException
    {
        public override string Code => "account_locked";

        public AccountLockedException() : base("Account is locked") { }

        public AccountLockedException(string message) : base(message)
        {
        }

        public AccountLockedException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
