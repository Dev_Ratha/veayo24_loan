﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class AgeLimitException : BaseException
    {
        public override string Code => "age_limited";
        public AgeLimitException() : base("User age not meet our policy") { }

        public AgeLimitException(string message) : base(message)
        {
        }

        public AgeLimitException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }
}
