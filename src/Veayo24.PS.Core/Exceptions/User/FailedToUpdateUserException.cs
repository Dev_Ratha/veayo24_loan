﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions.User
{
    public class FailedToUpdateUserException : BaseException
    {
        public override string Code => "unable_update_user";

        public FailedToUpdateUserException(string message) : base(message)
        {
        }

        public FailedToUpdateUserException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public FailedToUpdateUserException()
        {
        }
    }
}
