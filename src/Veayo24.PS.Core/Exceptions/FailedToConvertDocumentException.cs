﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class FailedToConvertDocumentException : BaseException
    {
        public override string Code => "failed_convert_document";
        public new int StatusCode { get; } = 500;

        public FailedToConvertDocumentException()
        {
        }

        public FailedToConvertDocumentException(string message) : base(message)
        {
        }

        public FailedToConvertDocumentException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
