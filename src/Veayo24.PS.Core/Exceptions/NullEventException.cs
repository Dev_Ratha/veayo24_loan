﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class NullEventException : BaseException
    {
        public override string Code => "null_event";
        public new int StatusCode { get; } = 500;

        public NullEventException()
        {
        }

        public NullEventException(string message) : base(message)
        {
        }

        public NullEventException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
