﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class NullCommandException : BaseException
    {
        public override string Code => "null_command";

        public NullCommandException(string message) : base(message)
        {
        }

        public NullCommandException(string message, Exception innerException) : base(message, innerException)
        {
        }

        public NullCommandException()
        {
        }
    }
}
