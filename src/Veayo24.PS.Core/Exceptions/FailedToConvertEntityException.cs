﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class FailedToConvertEntityException : BaseException
    {
        public override string Code => "failed_convert_entity";
        public new int StatusCode { get; } = 500;

        public FailedToConvertEntityException()
        {
        }

        public FailedToConvertEntityException(string message) : base(message)
        {
        }

        public FailedToConvertEntityException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
