﻿using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class UnAuthorizedRequestException : BaseException
    {
        public override string Code => "unauthorized";

        public UnAuthorizedRequestException() : base("Attempted to perform an unauthorized operation.") { }

        public UnAuthorizedRequestException(string message) : base(message)
        {
        }

        public UnAuthorizedRequestException(string message, System.Exception innerException) : base(message, innerException)
        {
        }
    }

}
