﻿using System;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class ForbiddenAccessResourceException : BaseException
    {
        public override string Code => "forbidden";
        public ForbiddenAccessResourceException(string msg) : base(msg, 403) { }

        public ForbiddenAccessResourceException()
        {
        }

        public ForbiddenAccessResourceException(string message, Exception innerException) : base(message, innerException)
        {
        }
    }
}
