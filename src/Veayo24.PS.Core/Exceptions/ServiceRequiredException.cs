﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.Exceptions;

namespace Veayo24.PS.Core.Exceptions
{
    public class ServiceRequiredException : BaseException
    {
        public override string Code => "service_required";
        public ServiceRequiredException(string serviceName) : base($"Service name {serviceName} is required!")
        {
        }
    }
}
