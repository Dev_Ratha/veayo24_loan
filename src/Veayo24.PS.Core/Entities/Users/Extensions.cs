﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Dto.Users;

namespace Veayo24.PS.Core.Entities.Users
{
    public static class Extensions
    {
        public static UserDto AsDto(this UserEntity x)
            => new UserDto()
            {
                BranchId = x.BranchId,
                CreatedAt = x.CreatedAt,
                FullName = x.FullName,
                Id = x.Id,
                MediaId = x.MediaId,
                Role = x.Role,
                UpdatedAt = x.UpdatedAt,
                UserName = x.UserName
            };
    }
}
