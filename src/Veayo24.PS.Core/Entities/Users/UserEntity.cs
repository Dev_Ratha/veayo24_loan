﻿using System;
using System.Security.Claims;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Identity;
using Veayo24.PS.Core.Exceptions.User;
using Veayo24.PS.Lib.Data;
using Veayo24.PS.Lib.Jwt;

namespace Veayo24.PS.Core.Entities.Users
{
    public class UserEntity : IEntity
    {
        public long Id { get; set; }
        public string FullName { get; set; }
        public string UserName { get; set; }
        public string Password { get; set; }
        public string PasswordHash { get; set; }
        public long? UserRoleId { get; set; }
        public string Role { get; set; }
        public string Token { get; set; }
        public long? MediaId { get; set; }
        public DateTime? CreatedAt { get; set; }
        public DateTime? UpdatedAt { get; set; }
        public long? BranchId { get; set; }
        public bool IsActive { get; set; }

        public UserEntity(
            long id,
            string fullName,
            string userName,
            string password,
            long? userRoleId,
            string role,
            string token,
            long? mediaId,
            DateTime? createdAt,
            DateTime? updatedAt,
            long? branchId,
            bool isActive)
        {
            Id = id;
            FullName = fullName;
            UserName = userName;
            Password = password;
            Role = role;
            Token = token;
            MediaId = mediaId;
            CreatedAt = createdAt;
            UpdatedAt = updatedAt;
            BranchId = branchId;
            IsActive = isActive;
            UserRoleId = userRoleId;
        }

        public void SetPassword(string password, IPasswordHasher<UserEntity> passwordHasher)
        {
            if (string.IsNullOrWhiteSpace(password) || passwordHasher is null)
                throw new InvalidPasswordException();

            PasswordHash = passwordHasher.HashPassword(this, password);
        }
        public bool ValidatePassword(string password, IPasswordHasher<UserEntity> passwordHasher)
            => passwordHasher?.VerifyHashedPassword(this, Password, password) != PasswordVerificationResult.Failed;
        public string CreateToken(ITokenProvider<UserEntity> tokenProvider)
            => tokenProvider?.CreateToken(new Claim[] {
                   new Claim(ClaimTypes.Name, Id.ToString()),
                   new Claim("x-branch", BranchId?.ToString() ?? "0"),
            }) ?? throw new FailedToCreateTokenException();
        public void SetToken(string token, IDataProtector protector)
            => Token = protector.Protect(token);
    }
}
