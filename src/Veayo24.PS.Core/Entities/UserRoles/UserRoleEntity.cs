﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.Data;

namespace Veayo24.PS.Core.Entities.UserRoles
{
    public class UserRoleEntity : IEntity
    {
        public long Id { get; set; }
        public string Role { get; set; }
        public long? BranchId { get; set; }

        public UserRoleEntity(long id, string role, long? branchId)
        {
            Id = id;
            Role = role;
            BranchId = branchId;
        }
    }
}
