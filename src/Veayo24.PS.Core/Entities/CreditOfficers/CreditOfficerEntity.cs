﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Lib.Data;

namespace Veayo24.PS.Core.Entities.CreditOfficers;

public class CreditOfficerEntity : IEntity
{
    public long Id { get; set; }
    public string Name { get; set; }
    public string NameLatin { get; set; }
    public string Position { get; set; }
    public bool IsProbation { get; set; }
    public bool IsLRO { get; set; }
    public bool IsActive { get; set; }
    public DateTime CreatedAt { get; set; }
    public DateTime UpdatedAt { get; set; }
    public long BranchId { get; set; }
    public CreditOfficerEntity(
        long id,
        string name,
        string nameLatin,
        string position,
        bool isProbation,
        bool isLRO,
        bool isActive,
        DateTime createdAt,
        DateTime updatedAt,
        long branchId)
    {
        Id = id;
        Name = name;
        NameLatin = nameLatin;
        Position = position;
        IsProbation = isProbation;
        IsLRO = isLRO;
        IsActive = isActive;
        CreatedAt = createdAt;
        UpdatedAt = updatedAt;
        BranchId = branchId;
    }
}
