﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.Users;
using Veayo24.PS.Lib.Data;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Core.Repositories.Users
{
    public interface IUserRepository : IRepository<UserEntity>
    {
        Task AddAsync(UserEntity entity);
        Task UpdateAsync(UserEntity entity);
        Task DeleteAsync(UserEntity entity);
        Task DeleteAsync(long id);
        Task<UserEntity?> GetAsync(long id);
        Task<UserEntity?> GetByUserNameAsync(string userName);
        Task<Pagination<UserEntity>> GetUserPageByBranch(long? branchId, int pageid, int result);

    }
}
