﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.CreditOfficers;
using Veayo24.PS.Lib.Data;
using Veayo24.PS.Lib.EntityFramework;

namespace Veayo24.PS.Core.Repositories
{
    public interface ICreditOfficerRepository : IRepository<CreditOfficerEntity>
    {
        Task<CreditOfficerEntity> AddAsync(CreditOfficerEntity creditOfficer);
        Task<CreditOfficerEntity> UpdateAsync(CreditOfficerEntity creditOfficer);
        Task<CreditOfficerEntity?> GetAsync(long id);
        Task<Pagination<CreditOfficerEntity>> GetPageAsync(long branchId, int pageId, int result);
    }
}
