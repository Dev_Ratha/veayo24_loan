﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Veayo24.PS.Core.Entities.UserRoles;
using Veayo24.PS.Lib.Data;

namespace Veayo24.PS.Core.Repositories
{
    public interface IUserRoleRepository : IRepository<UserRoleEntity>
    {
        Task<UserRoleEntity> AddAsync(UserRoleEntity userRoleEntity);
        Task<UserRoleEntity> UpdateAsync(UserRoleEntity userRoleEntity);
        Task DeleteAsync(long id);
        Task<UserRoleEntity?> GetAsync(long id);
        Task<IEnumerable<UserRoleEntity>> GetListAsync(long branchId);
    }
}
