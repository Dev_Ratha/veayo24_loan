namespace Veayo24.PS.Core.Utils
{
    public static class DateUtil
    {
        public static string LeadingZero(int value) => value < 10 ? $"0{value}" : value.ToString();
    }
}
