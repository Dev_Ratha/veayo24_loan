﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.Extensions.Localization;
using Veayo24.PS.Core.Resources.Languages.General;

namespace Veayo24.PS.Core.Utils
{
    public static class StringConjunction
    {
        private const string localizeKeyOther = "other";
        public static string JoinString(this IEnumerable<string> ls, IStringLocalizer<GeneralResource> stringLocalizer, string localizeKey = "and")
        {
            var c = ls.Count();
            switch (c)
            {
                case 0:
                    return string.Empty;
                case 1:
                    return ls.First();
                case 2:
                    return $"{ls.ElementAt(0)} {stringLocalizer[localizeKey]} {ls.ElementAt(1)}";
                default:
                    var and = stringLocalizer[localizeKey];
                    var other = stringLocalizer[localizeKeyOther];
                    var count = c - 2;
                    return $"{ls.ElementAt(0)}, {ls.ElementAt(1)}, {and} {count} {other}";
            }

        }
    }
}

