﻿using System;
namespace Veayo24.PS.Core.Utils
{
    public enum CircleType
    {
        Member,
        Place
    }

    public static class MembershipUtil
    {
        public static bool IsNullOrExpired(string customerId, DateTime? expiredAt)
        {
            return string.IsNullOrEmpty(customerId) || expiredAt < DateTime.Now;
        }

        public static string GetRemainingDays(DateTime currentDate, DateTime expiredAt)
        {
            TimeSpan diffDate = expiredAt - currentDate;

            if (diffDate.Days == 0)
            {
                string hours = DateUtil.LeadingZero(diffDate.Hours);
                string minutes = DateUtil.LeadingZero(diffDate.Minutes);

                return $"{hours}h {minutes}mn";
            }

            return $"{diffDate.Days} day(s)";
        }

        public static bool IsExpired(DateTime currentDate, DateTime expiredAt)
        {
            TimeSpan diffDate = expiredAt - currentDate;

            return diffDate.TotalDays <= 0;
        }
    }
}
