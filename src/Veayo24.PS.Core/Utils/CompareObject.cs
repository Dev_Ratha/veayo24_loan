﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Veayo24.PS.Core.Utils
{
    public class CompareObject
    {

        public void UpdatePropertyValue(dynamic val, dynamic newVal, string updateField)
        {
            PropertyInfo[]? propsNew = newVal?.GetType()?.GetProperties();
            PropertyInfo[]? propsOld = val?.GetType()?.GetProperties();
            if (propsNew is null || propsOld is null) return;
            foreach (var pOld in propsOld)
            {
                if (pOld.Name == updateField)
                {
                    foreach (var pNew in propsNew)
                    {
                        if (pNew.Name == pOld.Name && pOld.PropertyType == pNew.PropertyType)
                        {
                            pOld.SetValue(val, pNew.GetValue(newVal));
                        }
                    }
                }
            }

        }

        public Dictionary<string, dynamic> findDifference(dynamic oldValue, dynamic newValue, bool isDeep = true)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            PropertyInfo[]? propsNew = newValue?.GetType()?.GetProperties();
            PropertyInfo[]? propsOld = oldValue?.GetType()?.GetProperties();
            if (propsNew == null && propsOld == null) return result;

            if (propsNew is { })
            {
                foreach (PropertyInfo pNew in propsNew)
                {
                    if (propsOld == null)
                    {
                        var re = Process(oldValue, newValue, null, pNew) as Dictionary<string, dynamic>;
                        foreach (var key in re!.Keys)
                        {
                            result.Add(key, re[key]);
                        }
                    }
                    else
                    {
                        foreach (PropertyInfo pOld in propsOld)
                        {
                            var re = Process(oldValue, newValue, pOld, pNew, isDeep) as Dictionary<string, dynamic>;
                            foreach (var key in re!.Keys)
                            {
                                result.Add(key, re[key]);
                            }
                        }
                    }
                }
            }

            return result;
        }
        private Dictionary<string, dynamic> Process(dynamic oldValue, dynamic newValue, PropertyInfo pOld, PropertyInfo pNew, bool isDeep = true)
        {
            Dictionary<string, dynamic> result = new Dictionary<string, dynamic>();
            if (oldValue is null)
            {
                result.Add(pNew.Name, pNew.GetValue(newValue));
                return result;
            }

            if (pNew.Name == pOld.Name && pNew.PropertyType == pOld.PropertyType)
            {
                var oldVal = pOld.GetValue(oldValue);
                var newVal = pNew.GetValue(newValue);

                if (oldVal == null && newVal == null)
                    return result;
                if ((pOld.PropertyType.BaseType == typeof(object) && pOld.PropertyType != typeof(string))
                    || (pNew.PropertyType.BaseType == typeof(object) && pNew.PropertyType != typeof(string)))
                {
                    if (isDeep)
                    {
                        var tmp = findDifference(oldVal, newVal, isDeep);
                        if (tmp.Count > 0)
                            result.Add(pNew.Name, IgnoreNullFields(newVal));
                    }
                }
                else if (pOld.PropertyType.IsPrimitive && pNew.PropertyType.IsPrimitive || pOld.PropertyType == typeof(string))
                {
                    if (oldVal != newVal)
                        result.Add(pNew.Name, newVal);
                }
                else
                {
                    if (newVal is { })
                    {
                        var typeEnumerable = GetElementTypeOfEnumerable(newVal);
                        //var isPremativeCollection = IsPrimativeCollection(oldVal);
                        //PropertyInfo[] pArr = newVal?.GetType()?.GetProperties() ?? new { };
                        if (typeEnumerable is { })
                        {
                            if (oldVal == null || oldVal!.Count != newVal.Count)
                            {
                                result.Add(pNew.Name, newVal);
                            }
                            else if (oldVal != null || oldVal!.Count == newVal.Count)
                            {
                                foreach (var x in oldVal!)
                                {
                                    var isHad = false;
                                    foreach (var y in newVal)
                                    {
                                        if (x!.Equals(y))
                                        {
                                            isHad = true;
                                        }
                                    }
                                    if (!isHad)
                                    {
                                        result.Add(pNew.Name, newVal);
                                        break;
                                    }
                                }

                            }
                        }

                    }
                }

            }
            return result;
        }

        private Dictionary<string, dynamic>? IgnoreNullFields(dynamic val)
        {
            var dic = new Dictionary<string, dynamic>();
            PropertyInfo[]? propsNew = val?.GetType()?.GetProperties();
            if (propsNew is null) return null;
            foreach (var pNew in propsNew)
            {
                var value = pNew.GetValue(val);
                if (value != null)
                    dic.Add(pNew.Name, value);
            }
            return dic;
        }

        public Type? GetElementTypeOfEnumerable(object o)
        {
            if (o == null) return null;
            if (o.GetType() == typeof(string)) return null;
            var enumerable = o as IEnumerable;
            // if it's not an enumerable why do you call this method all ?
            if (enumerable == null)
                return null;

            Type[] interfaces = enumerable.GetType().GetInterfaces();

            return (from i in interfaces
                    where i.IsGenericType && i.GetGenericTypeDefinition() == typeof(IEnumerable<>)
                    select i.GetGenericArguments()[0]).FirstOrDefault();
        }

        public bool IsPrimativeCollection(dynamic val)
        {
            var valType = GetElementTypeOfEnumerable(val);
            if (valType == null) return false;
            Type[] types = { typeof(double), typeof(string), typeof(float), typeof(decimal), typeof(int), typeof(byte), typeof(String) };
            return types.Any(x => x == valType);
        }
    }
    public class CompareResult
    {
        public string Action { get; set; }
        public dynamic Field { get; set; }

        public CompareResult(string action, dynamic field)
        {
            Action = action;
            Field = field;
        }
    }
}
