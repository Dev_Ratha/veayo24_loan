﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Veayo24.PS.Core.Exceptions;

namespace Veayo24.PS.Core.Middlewares
{
    public sealed class AuthorizationRequestHandlerMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public AuthorizationRequestHandlerMiddleware(
            RequestDelegate next,
            ILogger<ErrorHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }

        public Task Invoke(HttpContext context)
        {
            var path = context.Request.Path.Value;
            return _next(context);
        }
    }
}
