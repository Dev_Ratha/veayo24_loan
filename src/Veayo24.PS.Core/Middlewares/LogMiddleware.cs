﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Veayo24.PS.Core.Exceptions;

namespace Veayo24.PS.Core.Middlewares
{
    public class LogMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<ErrorHandlerMiddleware> _logger;

        public LogMiddleware(
            RequestDelegate next,
            ILogger<ErrorHandlerMiddleware> logger)
        {
            _next = next;
            _logger = logger;
        }
        public Task Invoke(HttpContext context)
        {
            var clientId = context.Request.Headers["X-ClientId"].ToString();
            var clientIp = context.Request.Headers["X-Real-IP"].ToString();
            var userId = context.User.Identity?.Name;


            _logger.LogInformation("Requested from ip: {@IP}, client id: {@ClientId}, user id: {@UserId}", clientIp ?? string.Empty, clientId ?? string.Empty, userId ?? string.Empty);
            return _next(context);
        }

    }
}
