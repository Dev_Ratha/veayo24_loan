﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;

namespace Veayo24.PS.Core.Dto.Paginations;

public class PaginationDto
{
    [DefaultValue(1)]
    public int? PageId { get; set; } = 1;
    [DefaultValue(10)]
    public int? Result { get; set; } = 10;
    [DefaultValue("")]
    public string? Search { get; set; } = string.Empty;
    [DefaultValue("")]
    public string? Sort { get; set; } = string.Empty;
    public PaginationDto()
    {

    }
    public PaginationDto(int? pageId, int? result, string search, string sort)
    {
        PageId = pageId;
        Result = result;
        Search = search;
        Sort = sort;
    }
}
