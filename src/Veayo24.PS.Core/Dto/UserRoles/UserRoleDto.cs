﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Veayo24.PS.Core.Dto.UserRoles
{
    public class UserRoleDto
    {
        public long? Id { get; set; }
        public string Role { get; set; }
    }
}
