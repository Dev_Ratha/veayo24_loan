﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Veayo24.PS.Core.Resources.Languages {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "16.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class NotificationResource {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal NotificationResource() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Veayo24.PS.Core.Resources.Languages.NotificationResource", typeof(NotificationResource).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to and.
        /// </summary>
        public static string and {
            get {
                return ResourceManager.GetString("and", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to sent you a contact request.
        /// </summary>
        public static string contact_request_1 {
            get {
                return ResourceManager.GetString("contact_request_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to request your live location.
        /// </summary>
        public static string live_location_1 {
            get {
                return ResourceManager.GetString("live_location_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;b&gt;{0}&lt;/b&gt; approved &lt;b&gt;{1}&lt;/b&gt; place in &lt;b&gt;{2}&lt;/b&gt; circle..
        /// </summary>
        public static string map_place_approved_1 {
            get {
                return ResourceManager.GetString("map_place_approved_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;b&gt;{0}&lt;/b&gt; added a new place to &lt;b&gt;{1}&lt;/b&gt; circle..
        /// </summary>
        public static string map_place_created_1 {
            get {
                return ResourceManager.GetString("map_place_created_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;b&gt;{0}&lt;/b&gt; added a {1} new place to &lt;b&gt;{2}&lt;/b&gt; circle..
        /// </summary>
        public static string map_place_created_2 {
            get {
                return ResourceManager.GetString("map_place_created_2", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;b&gt;{0}&lt;/b&gt; removed &lt;b&gt;{1}&lt;/b&gt; place from &lt;b&gt;{2}&lt;/b&gt; circle..
        /// </summary>
        public static string map_place_removed_1 {
            get {
                return ResourceManager.GetString("map_place_removed_1", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to &lt;b&gt;{0}&lt;/b&gt; share {1} timeline with you in &lt;b&gt;{2}&lt;/b&gt; circle on &lt;b&gt;{3}&lt;/b&gt;..
        /// </summary>
        public static string share_timeline_1 {
            get {
                return ResourceManager.GetString("share_timeline_1", resourceCulture);
            }
        }
    }
}
