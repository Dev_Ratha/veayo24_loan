﻿using System.Collections.Generic;
using Veayo24.PS.Lib.MediaUpload;

namespace Veayo24.PS.Core
{
    public static class AppSettings
    {
        public static readonly int DefaultThumbnailHeight = 200;
        public static readonly string CurrentMediaUploadProvider = MediaUploadProvider.Cloudinary;

        //header middleware
        public static readonly Dictionary<string, string> ClientHeaders = new Dictionary<string, string>() {
            {"83n3zJAW", "Android"},
            {"DTux6jkb", "iOS"},
            {"Y685rYs2", "Web"},
            {"YnBJ7QgZ", "Other"}
        };

    }
}
