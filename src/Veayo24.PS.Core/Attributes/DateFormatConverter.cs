﻿using Newtonsoft.Json.Converters;

namespace Veayo24.PS.Core.Attributes
{
    public class DateFormatConverter : IsoDateTimeConverter
    {
        public DateFormatConverter(string format)
        {
            DateTimeFormat = format;
        }
    }
}
