﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.Extensions.Hosting;

namespace Veayo24.PS.Core.Attributes
{
    public class OnlyDevelopmentModeAttribute : ActionFilterAttribute
    {
        private readonly IHostEnvironment _hosting;

        public OnlyDevelopmentModeAttribute(IHostEnvironment hosting)
        {
            _hosting = hosting;
        }
        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        public override void OnActionExecuting(ActionExecutingContext context)
        {

            if (_hosting.IsProduction())
            {
                context.Result = new NotFoundResult();
                return;
            }
        }
    }
}
